package com.MutiModule.common.utils.jdbc.vo;

public class StatusVO {

	private Integer status;
	
	private ResultVO result;
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ResultVO getResult() {
		return result;
	}

	public void setResult(ResultVO result) {
		this.result = result;
	}

}
