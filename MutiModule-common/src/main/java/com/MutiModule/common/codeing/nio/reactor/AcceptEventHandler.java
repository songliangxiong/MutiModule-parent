/**
 * File : AcceptEventHandler.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年7月5日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.codeing.reactor <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.codeing.nio.reactor;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * ClassName : AcceptEventHandler <br/>
 * Function : TODO ADD FUNCTION. <br/>
 * Reason : TODO ADD REASON. <br/>
 * Date : 2017年7月5日 下午1:33:16 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class AcceptEventHandler implements EventHandler {
	private Selector demultiplexer;

	public AcceptEventHandler(Selector demultiplexer) {
		this.demultiplexer = demultiplexer;
	}

	public void handleEvent(SelectionKey handle) throws Exception {
		System.out.println("===== AcceptEventHandler.java Accept Event Handler =====");
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) handle.channel();
		SocketChannel socketChannel = serverSocketChannel.accept();
		if (socketChannel != null) {
			socketChannel.configureBlocking(false);
			socketChannel.register(demultiplexer, SelectionKey.OP_READ);
		}

	}
}
