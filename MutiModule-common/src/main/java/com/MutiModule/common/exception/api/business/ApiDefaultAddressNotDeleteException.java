/**
 * File : ApiDefaultAddressNotDeleteException.java <br/>
 * Author : lenovo <br/>
 * Version : 1.1 <br/>
 * Date : 2017年4月12日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.exception.api.business <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.exception.api.business;

import com.MutiModule.common.exception.api.base.ApiException;
import com.MutiModule.common.exception.api.base.enums.ApiErrorCodeEnum;

/**
 * ClassName : ApiDefaultAddressNotDeleteException <br/>
 * Function : 具体的业务逻辑的异常类， 禁止删除默认的用户地址数据. <br/>
 * Reason : 具体的业务逻辑的异常类， 禁止删除默认的用户地址数据. <br/>
 * Date : 2017年4月12日 下午6:12:46 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

/**
 * 
 * 类名：ApiDefaultAddressNotDeleteException  <br />
 *
 * 功能：	具体的异常类文件，禁止删除默认的地址信息，传参message字段，默认指定异常错误码为  ApiErrorCodeEnum.FORBIDDEN_DELETE
 *
 * @author : alexgaoyh <br />
 * @Date : 2017年4月12日 下午6:19:13  <br />
 * @version : 2017年4月12日 <br />
 */
public class ApiDefaultAddressNotDeleteException extends ApiException {

	/**  
	 * serialVersionUID : 
	 */
	private static final long serialVersionUID = 1L;

	public ApiDefaultAddressNotDeleteException(String message) {
		super(ApiErrorCodeEnum.FORBIDDEN_DELETE, message);
	}
}
