package com.MutiModule.common.exception;

/**
 * 数据转换异常
 * @author alexgaoyh
 *
 */
public class MyNumberFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyNumberFormatException() {
		super();
	}

	public MyNumberFormatException(String msg) {
		super(msg);
	}

	public MyNumberFormatException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public MyNumberFormatException(Throwable cause) {
		super(cause);
	}
}