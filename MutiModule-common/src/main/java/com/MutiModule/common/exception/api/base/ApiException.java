/**
 * File : ApiException.java <br/>
 * Author : lenovo <br/>
 * Version : 1.1 <br/>
 * Date : 2017年4月12日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.exception.api <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.exception.api.base;

import com.MutiModule.common.exception.api.base.enums.ApiErrorCodeEnum;

/**
 * ClassName : ApiException <br/>
 * Function : 通用的API超类异常. <br/>
 * Reason : 通用的API超类异常. <br/>
 * Date : 2017年4月12日 下午5:56:57 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

/**
 * 
 * 类名：ApiException  <br />
 *
 * 功能：	通用的异常处理类，通用类，具体的异常可以继承此类，指定默认的异常错误码
 *
 * @author : alexgaoyh <br />
 * @Date : 2017年4月12日 下午6:16:56  <br />
 * @version : 2017年4月12日 <br />
 */
public class ApiException extends RuntimeException {

	/**
	 * serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;

	protected ApiErrorCodeEnum errorCode;

	protected Object data;

	public ApiException(ApiErrorCodeEnum errorCode, String message, Object data, Throwable e) {
		super(message, e);
		this.errorCode = errorCode;
		this.data = data;
	}

	public ApiException(ApiErrorCodeEnum errorCode, String message, Object data) {
		this(errorCode, message, data, null);
	}

	public ApiException(ApiErrorCodeEnum errorCode, String message) {
		this(errorCode, message, null, null);
	}

	public ApiException(String message, Throwable e) {
		this(null, message, null, e);
	}

	public ApiException() {

	}

	public ApiException(Throwable e) {
		super(e);
	}

	public ApiErrorCodeEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ApiErrorCodeEnum errorCode) {
		this.errorCode = errorCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
