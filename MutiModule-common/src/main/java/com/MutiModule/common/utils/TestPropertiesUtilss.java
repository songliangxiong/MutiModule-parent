package com.MutiModule.common.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * test.properties 	文件读取，并且缓存配置文件的值到内存中
 * 方法不仅能够缓存配置文件内容，还能够做到自动加载配置文件的内容到内存，使用者完全不用考虑手动加载的过程，只需要在需要用到的地方直接调用 TestProperties 的get方法
 * [例：String value1 = TestPropertiesUtilss.getKey1()]就可以了（因为是静态方法，不用创建对象），
 * 这样如果内存中有缓存，函数就会直接读取内存中的数据，节省时间，如果没有缓存系统也会自动加载！！！
 * @author alexgaoyh
 *
 */
public class TestPropertiesUtilss {

	/**
	 * key1
	 */
	private static String key1 = null;
	
	/**
	 * key2
	 */
	private static String key2 = null;
	
	static{
		
        loads();
        
    }
	
    synchronized static public void loads(){
    	
        if(StringUtilss.isEmpty(key1) || StringUtilss.isEmpty(key2)){
            InputStream is = TestPropertiesUtilss.class.getResourceAsStream("/test.properties");
            Properties dproperties = new Properties();
            try {
            	dproperties.load(is);
                        
                key1 = dproperties.getProperty("key1").toString();
                key2 = dproperties.getProperty("key2").toString();  
                    
            }
            catch (Exception e) {
                System.err.println("不能读取属性文件. " + "请确保 test.properties 在CLASSPATH指定的路径中");
            }
        }
        
    }

	public static String getKey1() {
		return key1;
	}

	public static String getKey2() {
		return key2;
	}
    
}
