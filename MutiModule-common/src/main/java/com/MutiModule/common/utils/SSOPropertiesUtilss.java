package com.MutiModule.common.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * util类保存了SSO单点登陆对应的配置文件的部分参数， 读取的文件为 ： sso.properties 文件，
 * @author alexgaoyh
 *
 */
public class SSOPropertiesUtilss {

	/**
	 * SSO 对应的 登陆页面部分
	 */
	private static String SSOLoginPage;
	
	/**
	 * SSO 对应的 cookieName部分
	 */
	private static String CookieName;
	
	/**
	 * SSO 对应的 cookiePath部分，
	 */
	private static String DomainName;
	
	static{
		
        loads();
        
    }
	
    synchronized static public void loads(){
    	
        if(StringUtilss.isEmpty(SSOLoginPage) || StringUtilss.isEmpty(CookieName) || StringUtilss.isEmpty(DomainName)){
            InputStream is = SSOPropertiesUtilss.class.getResourceAsStream("/sso.properties");
            Properties dproperties = new Properties();
            try {
            	dproperties.load(is);
                        
            	SSOLoginPage = dproperties.getProperty("SSOLoginPage").toString();
            	CookieName = dproperties.getProperty("CookieName").toString();
            	DomainName = dproperties.getProperty("DomainName").toString();
            	
            }
            catch (Exception e) {
                System.err.println("不能读取属性文件. " + "请确保 sso.properties 在CLASSPATH指定的路径中");
            }
        }
        
    }

	public static String getSSOLoginPage() {
		return SSOLoginPage;
	}

	public static String getCookieName() {
		return CookieName;
	}

	public static String getDomainName() {
		return DomainName;
	}
    
    
}
