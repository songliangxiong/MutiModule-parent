/**
 * File : RegexUtilss.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年6月22日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.utils.regex <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.utils.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.MutiModule.common.exception.ValidateException;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.utils.regex.pattern.RegexPatternPool;

/**
 * ClassName : RegexUtilss <br/>
 * Function : 正则表达式工具类. <br/>
 * Reason : 正则表达式工具类. <br/>
 * Date : 2017年6月22日 下午4:56:03 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class RegexUtilss {
	
	/**
	 * 给定内容是否匹配正则
	 * @param regex 正则
	 * @param content 内容
	 * @return 正则为null或者""则不检查，返回true，内容为null返回false
	 */
	public static boolean isMatch(String regex, String content) {
		if(content == null) {
			//提供null的字符串为不匹配
			return false;
		}
		
		if(StringUtilss.isEmpty(regex)) {
			//正则不存在则为全匹配
			return true;
		}
		
		return Pattern.matches(regex, content);
	}

	/**
	 * 通过正则表达式验证
	 * 
	 * @param pattern 正则模式
	 * @param value 值
	 * @return 是否匹配正则
	 */
	public static boolean isMactchRegex(Pattern pattern, String value) {
		return isMatch("^[\u4E00-\u9FFF]+$", value);
	}

	/**
	 * 验证是否为英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @return 是否为英文字母 、数字和下划线
	 */
	public static boolean isGeneral(String value) {
		return isMactchRegex(RegexPatternPool.GENERAL, value);
	}
	
	/**
	 * 验证是否为英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateGeneral(String value, String errorMsg) throws ValidateException {
		if(false == isGeneral(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为给定长度范围的英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @param min 最小长度，负数自动识别为0
	 * @param max 最大长度，0或负数表示不限制最大长度
	 * @return 是否为给定长度范围的英文字母 、数字和下划线
	 */
	public static boolean isGeneral(String value, int min, int max) {
		String reg = "^\\w{" + min + "," + max + "}$";
		if (min < 0) {
			min = 0;
		}
		if (max <= 0) {
			reg = "^\\w{" + min + ",}$";
		}
		Pattern regPattern = Pattern.compile(reg);
		return isMactchRegex(regPattern, value);
	}
	
	/**
	 * 验证是否为给定长度范围的英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @param min 最小长度，负数自动识别为0
	 * @param max 最大长度，0或负数表示不限制最大长度
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateGeneral(String value, int min, int max, String errorMsg) throws ValidateException {
		if(false == isGeneral(value, min, max)){
			throw new ValidateException(errorMsg);
		}
	}
	
	/**
	 * 验证是否为给定最小长度的英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @param min 最小长度，负数自动识别为0
	 * @return 是否为给定最小长度的英文字母 、数字和下划线
	 */
	public static boolean isGeneral(String value, int min) {
		return isGeneral(value, min, 0);
	}
	
	/**
	 * 验证是否为给定最小长度的英文字母 、数字和下划线
	 * 
	 * @param value 值
	 * @param min 最小长度，负数自动识别为0
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateGeneral(String value, int min, String errorMsg) throws ValidateException {
		validateGeneral(value, min, 0, errorMsg);
	}
	
	/**
	 * 验证该字符串是否是数字
	 * 
	 * @param value 字符串内容
	 * @return 是否是数字
	 */
	public static boolean isNumber(String value) {
		if (StringUtilss.isBlank(value)) {
			return false;
		}
		return isMactchRegex(RegexPatternPool.NUMBERS, value);
	}
	
	/**
	 * 验证是否为数字
	 * 
	 * @param value 表单值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateNumbers(String value, String errorMsg) throws ValidateException {
		if(false == isNumber(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为货币
	 * 
	 * @param value 值
	 * @return 是否为货币
	 */
	public static boolean isMoney(String value) {
		return isMactchRegex(RegexPatternPool.MONEY, value);
	}
	
	/**
	 * 验证是否为货币
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateMoney(String value, String errorMsg) throws ValidateException {
		if(false == isMoney(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为邮政编码（中国）
	 * 
	 * @param value 值
	 * @return 是否为邮政编码（中国）
	 */
	public static boolean isZipCode(String value) {
		return isMactchRegex(RegexPatternPool.ZIP_CODE, value);
	}
	
	/**
	 * 验证是否为邮政编码（中国）
	 * 
	 * @param value 表单值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateZipCode(String value, String errorMsg) throws ValidateException {
		if(false == isZipCode(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为可用邮箱地址
	 * 
	 * @param value 值
	 * @return 否为可用邮箱地址
	 */
	public static boolean isEmail(String value) {
		return isMactchRegex(RegexPatternPool.EMAIL, value);
	}
	
	/**
	 * 验证是否为可用邮箱地址
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateEmail(String value, String errorMsg) throws ValidateException {
		if(false == isEmail(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为手机号码（中国）
	 * 
	 * @param value 值
	 * @return 是否为手机号码（中国）
	 */
	public static boolean isMobile(String value) {
		return isMactchRegex(RegexPatternPool.MOBILE, value);
	}
	
	/**
	 * 验证是否为手机号码（中国）
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateMobile(String value, String errorMsg) throws ValidateException {
		if(false == isMobile(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为身份证号码（18位中国）<br>
	 * 出生日期只支持到到2999年
	 * 
	 * @param value 值
	 * @return 是否为身份证号码（18位中国）
	 */
	public static boolean isCitizenId(String value) {
		return isMactchRegex(RegexPatternPool.CITIZEN_ID, value);
	}
	
	/**
	 * 验证是否为身份证号码（18位中国）<br>
	 * 出生日期只支持到到2999年
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateCitizenIdNumber(String value, String errorMsg) throws ValidateException {
		if(false == isCitizenId(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为生日<br>
	 * 
	 * @param year 年
	 * @param month 月
	 * @param day 日
	 * @return 是否为生日
	 */
	public static boolean isBirthday(int year, int month, int day) {
		//验证年
		int thisYear = DateUtils.thisYear();
		if(year < 1930 || year > thisYear){
			return false;
		}
		
		//验证月
		if (month < 1 || month > 12) {
			return false;
		}
		
		//验证日
		if (day < 1 || day > 31) {
			return false;
		}
		if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
			return false;
		}
		if (month == 2) {
			if (day > 29 || (day == 29 && false ==DateUtils.isLeapYear(year))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 验证是否为生日<br>
	 * 只支持以下几种格式：
	 * <ul>
	 * 	<li>yyyyMMdd</li>
	 * 	<li>yyyy-MM-dd</li>
	 * 	<li>yyyy/MM/dd</li>
	 * 	<li>yyyyMMdd</li>
	 * 	<li>yyyy年MM月dd日</li>
	 * </ul>
	 * 
	 * @param value 值
	 * @return 是否为生日
	 */
	public static boolean isBirthday(String value) {
		if(isMactchRegex(RegexPatternPool.BIRTHDAY, value)){
			Matcher matcher = RegexPatternPool.BIRTHDAY.matcher(value);
			if(matcher.find()){
				int year = Integer.parseInt(matcher.group(1));
				int month = Integer.parseInt(matcher.group(3));
				int day = Integer.parseInt(matcher.group(5));
				return isBirthday(year, month, day);
			}
		}
		return false;
	}
	
	/**
	 * 验证验证是否为生日<br>
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateBirthday(String value, String errorMsg) throws ValidateException {
		if(false == isBirthday(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为IPV4地址
	 * 
	 * @param value 值
	 * @return 是否为IPV4地址
	 */
	public static boolean isIpv4(String value) {
		return isMactchRegex(RegexPatternPool.IPV4, value);
	}
	
	/**
	 * 验证是否为IPV4地址
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateIpv4(String value, String errorMsg) throws ValidateException {
		if(false == isIpv4(value)){
			throw new ValidateException(errorMsg);
		}
	}
	
	/**
	 * 验证是否为中国车牌号
	 * 
	 * @param value 值
	 * @return 是否为IPV4地址
	 * @since 3.0.6
	 */
	public static boolean isPlateNumber(String value) {
		return isMactchRegex(RegexPatternPool.PLATE_NUMBER, value);
	}
	
	/**
	 * 验证是否为中国车牌号
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 * @since 3.0.6
	 */
	public static void validatePlateNumber(String value, String errorMsg) throws ValidateException {
		if(false == isPlateNumber(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为URL
	 * 
	 * @param value 值
	 * @return 是否为URL
	 */
	public static boolean isUrl(String value) {
		try {
			new java.net.URL(value);
		} catch (java.net.MalformedURLException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 验证是否为URL
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateUrl(String value, String errorMsg) throws ValidateException {
		if(false == isUrl(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为汉字
	 * 
	 * @param value 值
	 * @return 是否为汉字
	 */
	public static boolean isChinese(String value) {
		return isMactchRegex(RegexPatternPool.RE_IS_CHINESES, value);
	}
	
	/**
	 * 验证是否为汉字
	 * 
	 * @param value 表单值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateChinese(String value, String errorMsg) throws ValidateException {
		if(false == isChinese(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为中文字、英文字母、数字和下划线
	 * 
	 * @param value 值
	 * @return 是否为中文字、英文字母、数字和下划线
	 */
	public static boolean isGeneralWithChinese(String value) {
		return isMactchRegex(RegexPatternPool.GENERAL_WITH_CHINESE, value);
	}
	
	/**
	 * 验证是否为中文字、英文字母、数字和下划线
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateGeneralWithChinese(String value, String errorMsg) throws ValidateException {
		if(false == isGeneralWithChinese(value)){
			throw new ValidateException(errorMsg);
		}
	}

	/**
	 * 验证是否为UUID<br>
	 * 包括带横线标准格式和不带横线的简单模式
	 * 
	 * @param value 值
	 * @return 是否为UUID
	 */
	public static boolean isUUID(String value) {
		return isMactchRegex(RegexPatternPool.UUID, value) || isMactchRegex(RegexPatternPool.UUID_SIMPLE, value);
	}
	
	/**
	 * 验证是否为UUID<br>
	 * 包括带横线标准格式和不带横线的简单模式
	 * 
	 * @param value 值
	 * @param errorMsg 验证错误的信息
	 * @throws ValidateException 验证异常
	 */
	public static void validateUUID(String value, String errorMsg) throws ValidateException {
		if(false == isUUID(value)){
			throw new ValidateException(errorMsg);
		}
	}
	
}
