package com.alexgaoyh.MutiModule.persist.oneToMany;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.utils.JSONUtilss;
import com.alexgaoyh.MutiModule.persist.oneToMany.student.StudentExample;
import com.alexgaoyh.MutiModule.persist.oneToMany.student.StudentMapper;
import com.alexgaoyh.MutiModule.persist.oneToMany.view.StudentWithClassView;

public class StudentWithClassTest {

	private StudentMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (StudentMapper) ctx.getBean( "studentMapper" );
        
    }
	
	//@Test
	public void studentWithClassViewTest() {
		try {
			StudentExample example = new StudentExample();
			
			example.createCriteria().andClassIDEqualTo(1).andNamessLike("%"+"1"+"%");
			
			List<StudentWithClassView> list = mapper.selectWithClassEntityByExample(example);
			System.out.println(JSONUtilss.toJSon(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
