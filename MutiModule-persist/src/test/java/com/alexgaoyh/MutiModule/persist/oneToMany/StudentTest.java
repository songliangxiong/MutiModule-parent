package com.alexgaoyh.MutiModule.persist.oneToMany;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.alexgaoyh.MutiModule.persist.oneToMany.student.Student;
import com.alexgaoyh.MutiModule.persist.oneToMany.student.StudentMapper;

public class StudentTest {

	private StudentMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (StudentMapper) ctx.getBean( "studentMapper" );
        
    }
	
	//@Test
	public void insertTest() {
		Student stu = new Student();
		stu.setDeleteFlag(DeleteFlagEnum.NORMAL);
		stu.setCreateTime(new Date());
		stu.setNamess("stu3");
		stu.setClassID(1);
		mapper.insert(stu);
	}
}
