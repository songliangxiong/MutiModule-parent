package com.MutiModule.lucene.entity;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.FSDirectory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.lucene.LuceneUtils;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationExample;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationMapper;

/**
 * lucene 相关增量查询测试
 * 通过 iReader.maxDoc(); 获取到最大的docId， 之后取出这个docId 对应的Document 
 * 取出Document 包含的id值，这样，就可以获取到已经保存到索引文件中的最大的主键id，即可针对数据库中的值进行增量索引操作
 * @author alexgaoyh
 *
 */
public class LuceneIncreTest {

	private BaseLocationMapper mapper;
	
	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (BaseLocationMapper) ctx.getBean( "baseLocationMapper" );
        
    }

	//@Test
	public void testAll() throws IOException, ParseException, InvalidTokenOffsetsException {
		createIndex();

		getMaxDocId();
	}
	
	/**
	 * 生成索引文件
	 * @throws IOException
	 */
	public void createIndex() throws IOException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		BaseLocationExample example = new BaseLocationExample();
		example.or().andIdLessThan(201533);
		List<BaseLocation> list = mapper.selectByExample(example);
		
		for(BaseLocation bl : list) {
			Document doc = new Document();
			
			doc.add(new Field("id", bl.getId().toString(), TextField.TYPE_STORED));
			doc.add(new Field("createTime", bl.getCreateTime().toString(), TextField.TYPE_STORED));
			doc.add(new Field("nameCN", bl.getNameCN(), TextField.TYPE_STORED));
			
			iWriter.addDocument(doc);
		}
		
		LuceneUtils.closeIndexWriter(iWriter);
		
	}
	
	/**
	 * 获取索引文件中已经保存的最大的doc部分的主键id
	 * createIndex() 方法，保存了IdLessThan(201533) 的BaseLocation的值，
	 * 这时，需要查询出来已经保存的最大的id主键值，之后才可以进行索引文件的增量操作
	 * @throws IOException 
	 */
	public void getMaxDocId() throws IOException {
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
        
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		int maxDocId = iReader.maxDoc();
		
		Document doc = iReader.document(maxDocId - 1);
		
		String savedLastedId = doc.get("id");
		
		System.out.println(savedLastedId);
		
		LuceneUtils.closeIndexReader(iReader);
	}
	
}
