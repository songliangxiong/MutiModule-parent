/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : inspinia+

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2016-06-15 11:39:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `demo_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `demo_attachment`;
CREATE TABLE `demo_attachment` (
  `ID` bigint(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `demo_onewithmany_course`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_course`;
CREATE TABLE `demo_onewithmany_course` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_course
-- ----------------------------

-- ----------------------------
-- Table structure for `demo_onewithmany_course_student_rel`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_course_student_rel`;
CREATE TABLE `demo_onewithmany_course_student_rel` (
  `DEMO_ONEWITHMANY_COURSE_ID` varchar(32) NOT NULL DEFAULT '0',
  `DEMO_ONEWITHMANY_STUDENT_ID` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DEMO_ONEWITHMANY_COURSE_ID`,`DEMO_ONEWITHMANY_STUDENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_course_student_rel
-- ----------------------------

-- ----------------------------
-- Table structure for `demo_onewithmany_student`
-- ----------------------------
DROP TABLE IF EXISTS `demo_onewithmany_student`;
CREATE TABLE `demo_onewithmany_student` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo_onewithmany_student
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_dictionary`;
CREATE TABLE `sys_dict_dictionary` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `DELETE_FLAG` varchar(50) NOT NULL,
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `CONTENT` varchar(50) NOT NULL COMMENT '字典名称',
  `TYPE` varchar(50) NOT NULL COMMENT '类型',
  `PERFIX_CODE` varchar(50) NOT NULL COMMENT '编码前缀',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dict_dictionary_item`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_dictionary_item`;
CREATE TABLE `sys_dict_dictionary_item` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(32) NOT NULL,
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `DICT_ID` varchar(32) NOT NULL COMMENT '外键ID',
  `CONTENT` varchar(50) NOT NULL,
  `PARENT_ID` varchar(32) DEFAULT NULL COMMENT '自关联-父类Id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict_dictionary_item
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_sysman_resource`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_resource`;
CREATE TABLE `sys_sysman_resource` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(50) DEFAULT NULL,
  `CREATE_TIME` varchar(32) DEFAULT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `HREF` varchar(100) DEFAULT NULL,
  `RESOURCE_TYPE` int(1) DEFAULT NULL,
  `PARENT_ID` varchar(32) DEFAULT NULL,
  `LEVELS` int(1) DEFAULT NULL,
  `PARENT_IDS` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台资源权限表';

-- ----------------------------
-- Records of sys_sysman_resource
-- ----------------------------
INSERT INTO `sys_sysman_resource` VALUES ('11048197823472640', '0', null, '系统管理', '系统管理', '#', '0', null, '0', '');
INSERT INTO `sys_sysman_resource` VALUES ('11048198557017088', '0', null, '权限管理', '权限管理', '#', '0', '11048197823472640', '1', '11048197823472640,');
INSERT INTO `sys_sysman_resource` VALUES ('11048199436444672', '0', null, '后台用户管理', '后台用户管理', 'manager/sysman/sysmanUser/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysman_resource` VALUES ('11048200350082048', '0', null, '后台用户组管理', '后台用户组管理', 'manager/sysman/sysmanRole/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysman_resource` VALUES ('11048201254872064', '0', null, '后台资源管理', '后台资源管理', 'manager/sysman/sysmanResource/list', '0', '11048198557017088', '2', '11048197823472640,11048198557017088,');
INSERT INTO `sys_sysman_resource` VALUES ('11048209978500096', '0', '20160614065003', '添加', '后台用户管理-添加', 'manager/sysman/sysmanUser/add', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysman_resource` VALUES ('11048211508175872', '0', null, '修改', '后台用户管理-修改', 'manager/sysman/sysmanUser/edit', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysman_resource` VALUES ('11048212395664384', '0', null, '角色', '后台用户管理-角色', 'manager/sysman//sysmanUserRoleRel/userRoleRelList', '1', '11048199436444672', '3', '11048197823472640,11048198557017088,11048199436444672,');
INSERT INTO `sys_sysman_resource` VALUES ('11048249680208896', '0', '20160613090108', '添加', '后台用户组管理-添加', 'manager/sysman/sysmanRole/add', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysman_resource` VALUES ('11048250523788288', '0', null, '修改', '后台用户组管理-修改', 'manager/sysman/sysmanRole/edit', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysman_resource` VALUES ('11048251408131072', '0', null, '资源', '后台用户组管理-资源', 'manager/sysman/sysmanRoleResourceRel/roleResourceRelList', '1', '11048200350082048', '3', '11048197823472640,11048198557017088,11048200350082048,');
INSERT INTO `sys_sysman_resource` VALUES ('11048268803679232', '0', '20160613090113', '添加', '后台资源管理-添加', 'manager/sysman/sysmanResource/add', '1', '11048201254872064', '3', '11048197823472640,11048198557017088,11048201254872064,');
INSERT INTO `sys_sysman_resource` VALUES ('11048270217618432', '0', null, '修改', '后台资源管理-修改', 'manager/sysman/sysmanResource/edit', '1', '11048201254872064', '3', '11048197823472640,11048198557017088,11048201254872064,');
INSERT INTO `sys_sysman_resource` VALUES ('11087578173547520', '0', null, '测试功能', '测试功能', '#', '0', null, '0', '');
INSERT INTO `sys_sysman_resource` VALUES ('11087579158619136', '0', null, '一对多添加', '一对多添加', 'manager/demo/oneWithMany/courseStudent/list', '0', '11087578173547520', '1', '11087578173547520,');
INSERT INTO `sys_sysman_resource` VALUES ('11099611938563072', '0', null, '添加', '测试功能-一对多添加-添加按钮', 'manager/demo/oneWithMany/courseStudent/add', '1', '11087579158619136', '2', '11087578173547520,11087579158619136,');
INSERT INTO `sys_sysman_resource` VALUES ('11105064893752320', '0', '20160613090119', '修改', '测试功能-一对多添加-修改', 'manager/sysman/sysmanResource/edit', '1', '11087579158619136', '2', '11087578173547520,11087579158619136,');
INSERT INTO `sys_sysman_resource` VALUES ('11246955601142784', '0', null, '图片上传测试', '图片上传测试', 'manager/demo/uploadTest/list', '0', '11087578173547520', '1', '11087578173547520,');
INSERT INTO `sys_sysman_resource` VALUES ('11247016164205568', '0', null, '添加', '图上上传测试-添加', 'manager/demo/uploadTest/add', '1', '11246955601142784', '2', '11087578173547520,11246955601142784,');
INSERT INTO `sys_sysman_resource` VALUES ('11365562795697152', '0', null, '数据字典管理', '系统管理-数据字典管理-描述', '#', '0', '11048197823472640', '1', '11048197823472640,');
INSERT INTO `sys_sysman_resource` VALUES ('11365566972830720', '0', null, '数据字典', '系统管理-数据字典管理-数据字典-描述', 'manager/dict/dictionary/list', '0', '11365562795697152', '2', '11048197823472640,11365562795697152,');
INSERT INTO `sys_sysman_resource` VALUES ('11382221534405632', '0', null, '添加', '系统管理-数据字典管理-添加', 'manager/dict/dictionary/add', '1', '11365566972830720', '3', '11048197823472640,11365562795697152,11365566972830720,');
INSERT INTO `sys_sysman_resource` VALUES ('11382223521195008', '0', null, '修改', '系统管理-数据字典管理-修改', 'manager/dict/dictionary/edit', '1', '11365566972830720', '3', '11048197823472640,11365562795697152,11365566972830720,');

-- ----------------------------
-- Table structure for `sys_sysman_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_role`;
CREATE TABLE `sys_sysman_role` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(50) DEFAULT NULL,
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `PARENT_ID` varchar(32) DEFAULT NULL,
  `LEVELS` int(1) DEFAULT NULL,
  `PARENT_IDS` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Records of sys_sysman_role
-- ----------------------------
INSERT INTO `sys_sysman_role` VALUES ('11607918739923968', '0', '20160613085827', '系统管理员', '系统管理员-所有权限用户组', null, '0', null);

-- ----------------------------
-- Table structure for `sys_sysman_role_resource_rel`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_role_resource_rel`;
CREATE TABLE `sys_sysman_role_resource_rel` (
  `SYSMAN_ROLE_ID` varchar(32) NOT NULL DEFAULT '0',
  `SYSMAN_RESOURCE_ID` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SYSMAN_ROLE_ID`,`SYSMAN_RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sysman_role_resource_rel
-- ----------------------------
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048197823472640');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048198557017088');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048199436444672');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048200350082048');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048201254872064');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048209978500096');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048211508175872');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048212395664384');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048249680208896');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048250523788288');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048251408131072');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048268803679232');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11048270217618432');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11087578173547520');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11087579158619136');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11099611938563072');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11105064893752320');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11246955601142784');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11247016164205568');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11365562795697152');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11365566972830720');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11382221534405632');
INSERT INTO `sys_sysman_role_resource_rel` VALUES ('11607918739923968', '11382223521195008');

-- ----------------------------
-- Table structure for `sys_sysman_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_user`;
CREATE TABLE `sys_sysman_user` (
  `ID` varchar(32) NOT NULL,
  `DELETE_FLAG` varchar(50) NOT NULL DEFAULT '0',
  `CREATE_TIME` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(32) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户表结构';

-- ----------------------------
-- Records of sys_sysman_user
-- ----------------------------
INSERT INTO `sys_sysman_user` VALUES ('11607918739923967', '0', '20160613085301', 'admin@admin.com', 'admin@admin.com');

-- ----------------------------
-- Table structure for `sys_sysman_user_role_rel`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sysman_user_role_rel`;
CREATE TABLE `sys_sysman_user_role_rel` (
  `SYSMAN_USER_ID` varchar(32) NOT NULL DEFAULT '0',
  `SYSMAN_ROLE_ID` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SYSMAN_USER_ID`,`SYSMAN_ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sysman_user_role_rel
-- ----------------------------
INSERT INTO `sys_sysman_user_role_rel` VALUES ('11607918739923967', '11607918739923968');
