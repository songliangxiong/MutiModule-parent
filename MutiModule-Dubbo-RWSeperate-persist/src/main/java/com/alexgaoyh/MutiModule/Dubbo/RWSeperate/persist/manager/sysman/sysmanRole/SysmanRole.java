package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class SysmanRole extends BaseEntity implements Serializable {
	private String name;

	private String description;

	private String parentId;

	private Integer levels;

	private String parentIds;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getLevels() {
		return levels;
	}

	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	// add alexgaoyh 重写方法，用于判断两个对象的值是否相等
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SysmanRole other = (SysmanRole) obj;
		if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}
}