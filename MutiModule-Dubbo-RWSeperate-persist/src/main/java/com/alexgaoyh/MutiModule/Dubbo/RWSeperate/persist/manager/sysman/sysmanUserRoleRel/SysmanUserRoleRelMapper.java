package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;
import java.util.List;
import java.util.Map;

public interface SysmanUserRoleRelMapper {
	int deleteByPrimaryKey(SysmanUserRoleRelKey key);

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanUserRoleRelKey> selectListByMap(Map<Object, Object> map);

	int insert(SysmanUserRoleRelKey record);

	int insertSelective(SysmanUserRoleRelKey record);

	/**
	 * 根据用户userId 删除 所有角色id
	 * 
	 * @param sysmanUserId
	 *            用户id
	 * @return
	 */
	int deleteBySysmanUserId(String sysmanUserId);
}