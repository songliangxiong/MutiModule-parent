package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary;

import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithZtreeNodeView;

public interface DictDictionaryMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DictDictionary> selectListByMap(Map<Object, Object> map);

    int insert(DictDictionary record);

    int insertSelective(DictDictionary record);

    DictDictionary selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DictDictionary record);

    int updateByPrimaryKey(DictDictionary record);
    
	/**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合） 多方为
	 * DictDictionaryItem 类属性集合
	 * @param map

	 *            key部分为DictDictionary类的属性
	 * @return
	 */
	List<DictDictionaryWithItemView> selectDictionaryWithItemsByMap(Map<Object, Object> map);

	/**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合） 多方为
	 * ZTreeNodes 类属性集合
	 * @param map
	 *            key部分为DictDictionary类的属性
	 * @return
	 */
	List<DictDictionaryWithZtreeNodeView> selectDictionaryWithZTreeNodesListByMap(Map<Object, Object> map);

}