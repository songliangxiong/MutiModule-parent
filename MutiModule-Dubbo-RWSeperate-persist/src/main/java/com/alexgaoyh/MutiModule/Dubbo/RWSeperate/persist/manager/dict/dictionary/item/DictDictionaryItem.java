package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class DictDictionaryItem extends BaseEntity implements Serializable {
    private String dictId;

    private String content;

    private String parentId;

    private static final long serialVersionUID = 1L;

    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}