package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model;

import com.MutiModule.common.mybatis.annotation.*;
import com.MutiModule.common.mybatis.base.BaseEntity;
import com.MutiModule.common.vo.enums.sql.operation.SQLOperationModelEnum;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "sys_data_authority_model", namespace = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModelMapper", remarks = "数据权限-模型", aliasName = "sys_data_authority_model sys_data_authority_model")
public class SqlDataAuthorityOperationModel extends BaseEntity implements Serializable {
	/**
	 * 模型所属数据规则ID-外键关联,所属表字段为sys_data_authority_model.DATA_AUTHORITY_RULE_ID_FK
	 */
	@MyBatisColumnAnnotation(name = "DATA_AUTHORITY_RULE_ID_FK", value = "sys_data_authority_model_DATA_AUTHORITY_RULE_ID_FK", chineseNote = "模型所属数据规则ID-外键关联", tableAlias = "sys_data_authority_model")
	private String dataAuthorityRuleIdFk;

	/**
	 * 模型对象key，操作对象,所属表字段为sys_data_authority_model.SQL_MODEL_KEY
	 */
	@MyBatisColumnAnnotation(name = "SQL_MODEL_KEY", value = "sys_data_authority_model_SQL_MODEL_KEY", chineseNote = "模型对象key，操作对象", tableAlias = "sys_data_authority_model")
	private String sqlModelKey;

	/**
	 * 模型对象operation-模型对象操作符,所属表字段为sys_data_authority_model.SQL_MODEL_OPERATION
	 */
	@MyBatisColumnAnnotation(name = "SQL_MODEL_OPERATION", value = "sys_data_authority_model_SQL_MODEL_OPERATION", chineseNote = "模型对象operation-模型对象操作符", tableAlias = "sys_data_authority_model")
	private SQLOperationModelEnum sqlModelOperation;

	/**
	 * 模型对象value-模型对象操作值,所属表字段为sys_data_authority_model.SQL_MODEL_VALUE
	 */
	@MyBatisColumnAnnotation(name = "SQL_MODEL_VALUE", value = "sys_data_authority_model_SQL_MODEL_VALUE", chineseNote = "模型对象value-模型对象操作值", tableAlias = "sys_data_authority_model")
	private String sqlModelValue;

	private static final long serialVersionUID = 1L;

	public String getDataAuthorityRuleIdFk() {
		return dataAuthorityRuleIdFk;
	}

	public void setDataAuthorityRuleIdFk(String dataAuthorityRuleIdFk) {
		this.dataAuthorityRuleIdFk = dataAuthorityRuleIdFk;
	}

	public String getSqlModelKey() {
		return sqlModelKey;
	}

	public void setSqlModelKey(String sqlModelKey) {
		this.sqlModelKey = sqlModelKey;
	}

	public SQLOperationModelEnum getSqlModelOperation() {
		return sqlModelOperation;
	}

	public void setSqlModelOperation(SQLOperationModelEnum sqlModelOperation) {
		this.sqlModelOperation = sqlModelOperation;
	}

	public String getSqlModelValue() {
		return sqlModelValue;
	}

	public void setSqlModelValue(String sqlModelValue) {
		this.sqlModelValue = sqlModelValue;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", dataAuthorityRuleIdFk=").append(dataAuthorityRuleIdFk);
		sb.append(", sqlModelKey=").append(sqlModelKey);
		sb.append(", sqlModelOperation=").append(sqlModelOperation);
		sb.append(", sqlModelValue=").append(sqlModelValue);
		sb.append("]");
		return sb.toString();
	}
}