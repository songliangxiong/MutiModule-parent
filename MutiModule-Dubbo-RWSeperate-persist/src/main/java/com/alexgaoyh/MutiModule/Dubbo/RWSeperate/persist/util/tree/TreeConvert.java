package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.tree;

import java.util.ArrayList;
import java.util.List;

import com.MutiModule.common.vo.TreeNode;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;

public class TreeConvert {

	public static List<TreeNode> convertSysmanResourceListToTreeNodeList(List<SysmanResource> root, 
			List<SysmanResource> nodes) {
		List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
		
		for(SysmanResource _node : root) {
			TreeNode treeNode = getExtTree(_node , nodes);
			treeNodeList.add(treeNode);
		}
		return treeNodeList;
	}
	
	public static List<SysmanResource> getRootNodesList(List<SysmanResource> list) {
		//获取到所有顶级菜单节点
		List<SysmanResource> rootResourceList = new ArrayList<SysmanResource>();
		if(list != null && list.size() > 0) {
			for(SysmanResource _ztree : list ) {
				if(_ztree.getParentId() == null) {
					rootResourceList.add(_ztree);
				}
			}
		}
		return rootResourceList;
	}
	
	public static TreeNode getExtTree(SysmanResource node, List<SysmanResource> list){
		TreeNode tree = new TreeNode();
		tree.setId(Long.parseLong(node.getId()));
		tree.setParentId(node.getParentId() + "");
		tree.setParentAllIds(node.getParentIds());
		tree.setText(node.getName());
		tree.setHref(node.getHref());
		
		List<SysmanResource> childList = getChildList(node, list);
		
		if (childList.size() > 0) {
			List<TreeNode> lt = new ArrayList<TreeNode>();
			for(SysmanResource m : childList){
				lt.add(getExtTree(m, list));
			}
			tree.setChildren(lt);
		}
		return tree;
	}
	
	
	/**
	 * 从 list 整体集合中获取 node 的所有子元素
	 * @param node	某个特定节点，返回此节点下的所有子节点
	 * @param list	所有节点集合
	 * @return
	 */
	protected static List<SysmanResource> getChildList(SysmanResource node, List<SysmanResource> list) {
		List<SysmanResource> resultResourceList = new ArrayList<SysmanResource>();
		for( SysmanResource _zTreeNodes : list) {
			if(_zTreeNodes.getParentId() != null && node.getId() != null && _zTreeNodes.getParentId().equals(node.getId())) {
				resultResourceList.add(_zTreeNodes);
			}
		}
		return resultResourceList;
	}
	
	//-----------------------------------------------------------sysmanResource
	
	//-----------------------------------------------------------sysmanRole
	public static List<TreeNode> convertSysmanRoleListToTreeNodeList(List<SysmanRole> root, 
			List<SysmanRole> nodes) {
		List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
		
		for(SysmanRole _node : root) {
			TreeNode treeNode = getExtTreeRole(_node , nodes);
			treeNodeList.add(treeNode);
		}
		return treeNodeList;
	}
	
	public static TreeNode getExtTreeRole(SysmanRole node, List<SysmanRole> list){
		TreeNode tree = new TreeNode();
		tree.setId(Long.parseLong(node.getId()));
		tree.setParentId(node.getParentId() + "");
		tree.setParentAllIds(node.getParentIds());
		tree.setText(node.getName());
		
		List<SysmanRole> childList = getChildListRole(node, list);
		
		if (childList.size() > 0) {
			List<TreeNode> lt = new ArrayList<TreeNode>();
			for(SysmanRole m : childList){
				lt.add(getExtTreeRole(m, list));
			}
			tree.setChildren(lt);
		}
		return tree;
	}
	
	/**
	 * 从 list 整体集合中获取 node 的所有子元素
	 * @param node	某个特定节点，返回此节点下的所有子节点
	 * @param list	所有节点集合
	 * @return
	 */
	protected static List<SysmanRole> getChildListRole(SysmanRole node, List<SysmanRole> list) {
		List<SysmanRole> resultResourceList = new ArrayList<SysmanRole>();
		for( SysmanRole _zTreeNodes : list) {
			if(_zTreeNodes.getParentId() != null && node.getId() != null && _zTreeNodes.getParentId().equals(node.getId())) {
				resultResourceList.add(_zTreeNodes);
			}
		}
		return resultResourceList;
	}
	
	public static List<SysmanRole> getRootNodesRoleList(List<SysmanRole> list) {
		//获取到所有顶级菜单节点
		List<SysmanRole> rootResourceList = new ArrayList<SysmanRole>();
		if(list != null && list.size() > 0) {
			for(SysmanRole _ztree : list ) {
				if(_ztree.getParentId() == null) {
					rootResourceList.add(_ztree);
				}
			}
		}
		return rootResourceList;
	}
}
