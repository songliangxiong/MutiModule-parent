package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo;

import java.io.Serializable;

/**
 * 类的属性和属性对应的注释部分
 * @author alexgaoyh
 *
 */
public class SqlDataAuthorityModelVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3277635740775497275L;
	
	public SqlDataAuthorityModelVO(String attribute, String remarks) {
		this.attribute = attribute;
		this.remarks = remarks;
	}

	/**
	 * 类的属性部分
	 * 形如 学生表 Student 的其中一个属性ID 的属性 ‘ID’
	 */
	private String attribute;
	
	/**
	 * 类的属性的注释
	 * 形如 学生表 Student 的其中一个属性ID 的注释  ‘编号’
	 */
	private String remarks;

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
