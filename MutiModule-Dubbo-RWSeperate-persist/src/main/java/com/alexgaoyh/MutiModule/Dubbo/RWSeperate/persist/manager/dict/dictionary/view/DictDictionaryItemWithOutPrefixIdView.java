package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;

public class DictDictionaryItemWithOutPrefixIdView extends DictDictionaryItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6802264334165759168L;
	/**
	 * 保存到数据库中的 字典值 部分主键ID是包含前缀字段的，这里的readId,是不包含此前缀的 字典值 的值
	 */
	private String readId;

	public String getReadId() {
		return readId;
	}

	public void setReadId(String readId) {
		this.readId = readId;
	}
}
