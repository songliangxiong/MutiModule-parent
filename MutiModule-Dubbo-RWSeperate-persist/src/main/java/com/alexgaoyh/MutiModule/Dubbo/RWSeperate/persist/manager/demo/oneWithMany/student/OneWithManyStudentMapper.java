package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;
import java.util.List;
import java.util.Map;

public interface OneWithManyStudentMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<OneWithManyStudent> selectListByMap(Map<Object, Object> map);

    int insert(OneWithManyStudent record);

    int insertSelective(OneWithManyStudent record);

    OneWithManyStudent selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OneWithManyStudent record);

    int updateByPrimaryKey(OneWithManyStudent record);
    
    /**
     * 根据classId 删除班级下的所有studentList
     * @param classId	班级id
     * @return
     */
    int deleteStudentListByClassId(String classId);
}