package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.zTree.ZTreeNodesBigDataAsync;

public class ParamDataLocationTest {

	private ParamDataLocationMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (ParamDataLocationMapper) ctx.getBean( "paramDataLocationMapper" );
        
    }
	
	// @Test
	public void selectSingleZTreeNodeListByParentIdTest() {
		// 0 714441
		List<ZTreeNodesBigDataAsync> list = mapper.selectSingleZTreeNodeListByParentId("0");
		for (ZTreeNodesBigDataAsync paramDataLocation : list) {
			System.out.println(paramDataLocation.getIsParent());
		}
	}
}
