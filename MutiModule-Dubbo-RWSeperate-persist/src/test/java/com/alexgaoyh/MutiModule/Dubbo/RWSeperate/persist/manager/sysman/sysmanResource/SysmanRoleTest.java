package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRoleMapper;

public class SysmanRoleTest {

	private SysmanRoleMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (SysmanRoleMapper) ctx.getBean( "sysmanRoleMapper" );
        
    }
	
	// @Test
	public void selectTreeNodeBySysmanResourceIdTest() {
		List<SysmanRole> list = mapper.selectBySysmanUserId("682814769132081152");
	}
	
	// @Test
	public void removeList() {
		List<SysmanRole> allList  = mapper.selectListByMap(null);
		List<SysmanRole> selectedList = mapper.selectBySysmanUserId("682814769132081152");
		
		
		allList.removeAll(selectedList);
		System.out.println("allList = " + allList.size());
	}
}
