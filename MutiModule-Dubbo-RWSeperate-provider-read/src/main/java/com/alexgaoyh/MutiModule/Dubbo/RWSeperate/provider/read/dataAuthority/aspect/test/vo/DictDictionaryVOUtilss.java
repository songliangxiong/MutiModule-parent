package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictDictionaryVOUtilss {

	public static final Map<String, List<SqlModelVO>> operationMap = new HashMap<String, List<SqlModelVO>>();

	static {
		List<SqlModelVO> _operation = new ArrayList<SqlModelVO>();
		SqlModelVO _vo = new SqlModelVO("myLike_content", "like", "1");
		SqlModelVO _vo1 = new SqlModelVO("myLikeStart_content", "likeStart", "1");
		SqlModelVO _vo2 = new SqlModelVO("myLikeEnd_content", "likeEnd", "1");
		
		Long[] _ids = new Long[]{1L, 2L, 3L};
		SqlModelVO _vo3 = new SqlModelVO("myIn_content", "in", _ids);
		
		SqlModelVO _vo4 = new SqlModelVO("myGreater_content", "greater", "1");
		SqlModelVO _vo5 = new SqlModelVO("myLesser_content", "lesser", "1");
		
		
		_operation.add(_vo);
		_operation.add(_vo1);
		_operation.add(_vo2);
		_operation.add(_vo3);
		_operation.add(_vo4);
		_operation.add(_vo5);
		
		// operationMap.put("com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary", _operation);
	}
}
