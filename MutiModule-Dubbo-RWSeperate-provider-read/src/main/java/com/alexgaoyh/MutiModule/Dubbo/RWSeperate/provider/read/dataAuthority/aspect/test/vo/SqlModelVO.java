package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo;

/**
 * 	SQL 操作符
 * @author alexgaoyh
 *
 */
public class SqlModelVO {
	
	/**
	 * key
	 */
	private String prop;
	
	/**
	 * 操作符
	 */
	private String operation;
	
	/**
	 * value
	 * 如果是 in 语句的话，这里的values 值为 数组 
	 */
	private Object values;
	
	
	public SqlModelVO(String prop, String operation, Object values) {
		this.prop = prop;
		this.operation = operation;
		this.values = values;
	}

	
	public String getProp() {
		return prop;
	}


	public void setProp(String prop) {
		this.prop = prop;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public Object getValues() {
		return values;
	}


	public void setValues(Object values) {
		this.values = values;
	}
	
	
	
}
