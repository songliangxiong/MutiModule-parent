package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.sysman.sysmanRoleRsourceRle;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.write.ISysmanRoleResourceRelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelMapper;

@Service(value = "sysmanRoleResourceRelService")
public class SysmanRoleResourceRelServiceImpl implements ISysmanRoleResourceRelWriteService{
	
	@Resource(name = "sysmanRoleResourceRelMapper")
	private SysmanRoleResourceRelMapper mapper;

	@Override
	public int deleteByPrimaryKey(SysmanRoleResourceRelKey key) {
		return mapper.deleteByPrimaryKey(key);
	}

	@Override
	public int insert(SysmanRoleResourceRelKey record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysmanRoleResourceRelKey record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int deleteBySysmanRoleId(String sysmanRoleId) {
		return mapper.deleteBySysmanRoleId(sysmanRoleId);
	}

	@Override
	public int refreshSysmanRoleResourceRel(String sysmanRoleId, String sysmanResourceIds) {
		mapper.deleteBySysmanRoleId(sysmanRoleId);
		
		if(StringUtilss.isNotEmpty(sysmanResourceIds)) {
        	String[] _sysmanResourceArray = sysmanResourceIds.split(",");
        	for (int i = 0; i < _sysmanResourceArray.length; i++) {
        		SysmanRoleResourceRelKey _userRoleRelKey = new SysmanRoleResourceRelKey();
        		_userRoleRelKey.setSysmanRoleId(sysmanRoleId);
        		_userRoleRelKey.setSysmanResourceId(_sysmanResourceArray[i]);
        		mapper.insert(_userRoleRelKey);
        	}
        }
		return sysmanResourceIds.split(",").length;
	}

	@Override
	public int insertList(List<SysmanRoleResourceRelKey> list) {
		for(SysmanRoleResourceRelKey _key : list) {
			mapper.insert(_key);
		}
		return list.size();
	}


}
