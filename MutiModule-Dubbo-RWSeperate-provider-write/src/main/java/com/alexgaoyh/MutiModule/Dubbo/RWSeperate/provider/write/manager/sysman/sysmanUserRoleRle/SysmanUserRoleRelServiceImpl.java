package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.sysman.sysmanUserRoleRle;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.write.ISysmanUserRoleRelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelMapper;

@Service(value = "sysmanUserRoleRelService")
public class SysmanUserRoleRelServiceImpl implements ISysmanUserRoleRelWriteService {

	@Resource(name = "sysmanUserRoleRelMapper")
	private SysmanUserRoleRelMapper mapper;

	@Override
	public int deleteByPrimaryKey(SysmanUserRoleRelKey key) {
		return mapper.deleteByPrimaryKey(key);
	}

	@Override
	public int insert(SysmanUserRoleRelKey record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysmanUserRoleRelKey record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int deleteBySysmanUserId(String sysmanUserId) {
		return mapper.deleteBySysmanUserId(sysmanUserId);
	}

	@Override
	public int insertList(List<SysmanUserRoleRelKey> list) {
		for (SysmanUserRoleRelKey _userRoleRelKey : list) {
			mapper.insert(_userRoleRelKey);
		}
		return list.size();
	}

	@Override
	public int refreshUserRoleRel(String sysmanUserId, String sysmanRoleIds) {
		mapper.deleteBySysmanUserId(sysmanUserId);

		if (StringUtilss.isNotEmpty(sysmanRoleIds)) {
			String[] _sysmanRoleArray = sysmanRoleIds.split(",");
			for (int i = 0; i < _sysmanRoleArray.length; i++) {
				SysmanUserRoleRelKey _userRoleRelKey = new SysmanUserRoleRelKey();
				_userRoleRelKey.setSysmanUserId(sysmanUserId);
				_userRoleRelKey.setSysmanRoleId(_sysmanRoleArray[i]);
				mapper.insert(_userRoleRelKey);
			}
		}

		return sysmanRoleIds.split(",").length;
	}

}
