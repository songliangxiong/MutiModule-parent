package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class CodeGenerateUtilss {

	public static void main(String[] args) {
		demo();
	}

	public static void demo() {
		Map<String, Object> root = new HashMap<String, Object>();
		//子文件的包名
		root.put("packageName", "manager.data.authority.group");// 子包包名，整个包的路径为  
		//实体类名称
		root.put("className", "SqlDataAuthorityOperationGroup");// 类名称
		//实体类名称首字母小写，驼峰式
		root.put("smallClassName", "sqlDataAuthorityOperationGroup");// 类名称的首字母小写
		
		String workDir = (String) System.getProperties().get("user.dir");
		try {
			write(workDir, root);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void write(String workDir, Map<String, Object> input)
			throws Exception {
		String fileName = workDir + "/src/main/java/"
				+ input.get("packageName").toString().replaceAll("\\.", "/")
				+ "/write/" + input.get("className").toString()
				+ "ServiceImpl.java";
		File myFile = new File(fileName);
		myFile.getParentFile().mkdirs();
		myFile.createNewFile();
		buildFile("templete/write.ftl", fileName, input);
	}
	
	/**
	 * 生成文件
	 * 
	 * @param templateName
	 *            模板文件
	 * @param fileName
	 *            生成文件
	 * @param root
	 *            参数
	 */
	@SuppressWarnings("deprecation")
	private static void buildFile(String templateName, String fileName, Map<String, Object> root) {
		Configuration freemarkerCfg = new Configuration();
		freemarkerCfg.setClassForTemplateLoading(CodeGenerateUtilss.class, "/");
		freemarkerCfg.setEncoding(Locale.getDefault(), "UTF-8");
		Template template;
		try {
			template = freemarkerCfg.getTemplate(templateName);
			template.setEncoding("UTF-8");
			File htmlFile = new File(fileName);
			Writer out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(htmlFile), "UTF-8"));
			template.process(root, out);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
