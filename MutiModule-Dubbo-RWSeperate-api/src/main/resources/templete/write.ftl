package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className};

/**
 * ${className} 模块 写接口
 * @author alexgaoyh
 *
 */
public interface I${className}WriteService {

	int deleteByPrimaryKey(String id);

	int insert(${className} record);

	int insertSelective(${className} record);

	int updateByPrimaryKeySelective(${className} record);

	int updateByPrimaryKey(${className} record);
}
