package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.write;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;

/**
 * SysmanUserRoleRel 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISysmanUserRoleRelWriteService {

	int deleteByPrimaryKey(SysmanUserRoleRelKey key);

	int insert(SysmanUserRoleRelKey record);

	int insertSelective(SysmanUserRoleRelKey record);

    /**
     * 根据用户userId 删除 所有角色id
     * @param sysmanUserId	用户id
     * @return
     */
    int deleteBySysmanUserId(String sysmanUserId);
    
    /**
     * 批量插入操作
     * @param list
     * @return
     */
    int insertList(List<SysmanUserRoleRelKey> list);
    
    /**
     * 刷新用户实体与用户角色关联关系
     * @param sysmanUserId	用户实体主键id
     * @param sysmanRoleIds	用户角色资源id集合，以逗号为分隔符
     * @return
     */
    int refreshUserRoleRel(String sysmanUserId, String sysmanRoleIds);
}
