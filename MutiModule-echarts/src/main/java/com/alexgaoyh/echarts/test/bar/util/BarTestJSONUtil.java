package com.alexgaoyh.echarts.test.bar.util;

import java.util.ArrayList;
import java.util.List;

import com.github.abel533.echarts.AxisPointer;
import com.github.abel533.echarts.Grid;
import com.github.abel533.echarts.Label;
import com.github.abel533.echarts.Legend;
import com.github.abel533.echarts.Timeline;
import com.github.abel533.echarts.axis.Axis;
import com.github.abel533.echarts.axis.AxisLabel;
import com.github.abel533.echarts.axis.AxisLine;
import com.github.abel533.echarts.axis.AxisTick;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.SplitArea;
import com.github.abel533.echarts.axis.SplitLine;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.AxisType;
import com.github.abel533.echarts.code.LineType;
import com.github.abel533.echarts.code.Magic;
import com.github.abel533.echarts.code.MarkType;
import com.github.abel533.echarts.code.Orient;
import com.github.abel533.echarts.code.PointerType;
import com.github.abel533.echarts.code.Position;
import com.github.abel533.echarts.code.Tool;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.data.Data;
import com.github.abel533.echarts.data.PointData;
import com.github.abel533.echarts.feature.MagicType;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Bar;
import com.github.abel533.echarts.series.MarkLine;
import com.github.abel533.echarts.style.ItemStyle;
import com.github.abel533.echarts.style.LineStyle;
import com.github.abel533.echarts.style.TextStyle;
import com.github.abel533.echarts.style.itemstyle.Emphasis;
import com.github.abel533.echarts.style.itemstyle.Normal;

public class BarTestJSONUtil {

	public final static String generateObject_1() {
		GsonOption option = new GsonOption();
        option.title().text("某地区蒸发量和降水量").subtext("纯属虚构");
        option.tooltip().trigger(Trigger.axis);
        option.legend("蒸发量", "降水量");
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        option.calculable(true);
        option.xAxis(new CategoryAxis().data("1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"));
        option.yAxis(new ValueAxis());

        Bar bar = new Bar("蒸发量");
        bar.data(2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3);
        bar.markPoint().data(new PointData().type(MarkType.max).name("最大值"), new PointData().type(MarkType.min).name("最小值"));
        bar.markLine().data(new PointData().type(MarkType.average).name("平均值"));

        Bar bar2 = new Bar("降水量");
        bar2.data(2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3);
        bar2.markPoint().data(new PointData("年最高", 182.2).xAxis(7).yAxis(183).symbolSize(18), new PointData("年最低", 2.3).xAxis(11).yAxis(3));
        bar2.markLine().data(new PointData().type(MarkType.average).name("平均值"));

        option.series(bar, bar2);
        
        return option.toString();
	}
	
	
	public final static String generateObject_2() {
		GsonOption option = new GsonOption();
        option.title().text("某网站访问量统计").subtext("纯属虚构");
        	AxisPointer axisPointer = new AxisPointer();
        	axisPointer.setType(PointerType.shadow);
        option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
        
        option.legend("直接访问", "邮件营销", "联盟广告", "视频广告", "搜索引擎", "百度", "谷歌", "必应", "其他");
        option.toolbox().show(true).orient(Orient.vertical).x("right").y("center").feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar, Magic.stack, Magic.tiled).show(true), Tool.restore, Tool.saveAsImage);
        option.calculable(true);
        option.xAxis(new CategoryAxis().type(AxisType.category).data("周一", "周二", "周三", "周四", "周五", "周六", "周日"));
        option.yAxis(new ValueAxis());

        Bar bar1 = new Bar("直接访问");
        bar1.data(320, 332, 301, 334, 390, 330, 320);

        Bar bar2 = new Bar("邮件营销");
        bar2.setStack("广告");
        bar2.data(120, 132, 101, 134, 90, 230, 210);
        
        Bar bar3 = new Bar("联盟广告");
        bar3.setStack("广告");
        bar3.data(220, 182, 191, 234, 290, 330, 310);
        
        Bar bar4 = new Bar("视频广告");
        bar4.setStack("广告");
        bar4.data(150, 232, 201, 154, 190, 330, 410);
        
        Bar bar5 = new Bar("搜索引擎");
        bar5.data(862, 1018, 964, 1026, 1679, 1600, 1570);
        	ItemStyle itemStryle = new ItemStyle();
        	itemStryle.normal(new Normal().lineStyle(new LineStyle().type(LineType.dashed)));
        bar5.markLine().data(new PointData().type(MarkType.min), new PointData().type(MarkType.max)).itemStyle(itemStryle);
        
        Bar bar6 = new Bar("百度");
        bar6.setStack("搜索引擎");
        bar6.barWidth(5);
        bar6.data(620, 732, 701, 734, 1090, 1130, 1120);
        
        Bar bar7 = new Bar("谷歌");
        bar7.setStack("搜索引擎");
        bar7.data(120, 132, 101, 134, 290, 230, 220);
        
        Bar bar8 = new Bar("必应");
        bar8.setStack("搜索引擎");
        bar8.data(60, 72, 71, 74, 190, 130, 110);
        
        Bar bar9 = new Bar("其他");
        bar9.setStack("搜索引擎");
        bar9.data(62, 82, 91, 84, 109, 110, 120);

        option.series(bar1, bar2, bar3, bar4, bar5, bar6, bar7, bar8, bar9);
        
        return option.toString();
	}
	
	public final static String generateObject_3() {
		GsonOption option = new GsonOption();
        option.title().text("世界人口总量").subtext("数据来自网络");
        option.tooltip().trigger(Trigger.axis);
        option.legend("2011年", "2201年");
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        option.calculable(true);
        option.xAxis(new ValueAxis().boundaryGap(0D, 0.01D));
        option.yAxis(new CategoryAxis().data("巴西", "印尼", "美国", "印度", "中国", "世界人口(万)"));

        Bar bar = new Bar("2011年");
        bar.data(18203, 23489, 29034, 104970, 131744, 630230);

        Bar bar2 = new Bar("2012年");
        bar2.data(19325, 23438, 31000, 121594, 134141, 681807);

        option.series(bar, bar2);
        
        return option.toString();
	}
	
	public final static String generateObject_4() {
		GsonOption option = new GsonOption();
        option.title().text("bar4").subtext("数据来自网络");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    
        option.legend("直接访问", "邮件营销", "联盟广告", "视频广告", "搜索引擎");
        
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.calculable(true);
        option.xAxis(new ValueAxis());
        option.yAxis(new CategoryAxis().data("周一", "周二", "周三", "周四", "周五", "周六", "周日"));

        Bar bar1 = new Bar("直接访问");
        bar1.setStack("总量");
	        ItemStyle itemStryle1 = new ItemStyle();
	    	itemStryle1.normal(new Normal().label(new Label().show(true).position(Position.right)));
	    bar1.setItemStyle(itemStryle1);	
        bar1.data(320, 302, 301, 334, 390, 330, 320);

        Bar bar2 = new Bar("邮件营销");
        bar2.setStack("总量");
	        ItemStyle itemStryle2 = new ItemStyle();
	    	itemStryle2.normal(new Normal().label(new Label().show(true).position(Position.right)));
	    bar2.setItemStyle(itemStryle2);	
        bar2.data(320, 302, 301, 334, 390, 330, 320);
        
        Bar bar3 = new Bar("联盟广告");
        bar3.setStack("总量");
	        ItemStyle itemStryle3 = new ItemStyle();
	    	itemStryle3.normal(new Normal().label(new Label().show(true).position(Position.right)));
	    bar3.setItemStyle(itemStryle3);	
        bar3.data(220, 182, 191, 234, 290, 330, 310);
        
        Bar bar4 = new Bar("视频广告");
        bar4.setStack("总量");
	        ItemStyle itemStryle4 = new ItemStyle();
	    	itemStryle4.normal(new Normal().label(new Label().show(true).position(Position.right)));
	    bar4.setItemStyle(itemStryle4);	
        bar4.data(150, 212, 201, 154, 190, 330, 410);
        
        Bar bar5 = new Bar("搜索引擎");
        bar5.setStack("总量");
	        ItemStyle itemStryle5 = new ItemStyle();
	    	itemStryle5.normal(new Normal().label(new Label().show(true).position(Position.right)));
	    bar5.setItemStyle(itemStryle4);	
        bar5.data(820, 832, 901, 934, 1290, 1330, 1320);

        option.series(bar1, bar2, bar3, bar4, bar5);
        
        return option.toString();
	}
	
	public final static String generateObject_5() {
		GsonOption option = new GsonOption();
        option.title().text("收入-支出=利润").subtext("数据来自网络");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    
        option.legend("利润", "支出", "收入");
        
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.calculable(true);
        option.xAxis(new ValueAxis());
        option.yAxis(new CategoryAxis().data("周一", "周二", "周三", "周四", "周五", "周六", "周日"));

        Bar bar1 = new Bar("利润");
	        ItemStyle itemStryle1 = new ItemStyle();
	    	itemStryle1.normal(new Normal().label(new Label().show(true).position(Position.inside)));
	    bar1.setItemStyle(itemStryle1);	
        bar1.data(200, 170, 240, 244, 200, 220, 210);
        
        Bar bar2 = new Bar("收入");
	        ItemStyle itemStryle2 = new ItemStyle();
	    	itemStryle2.normal(new Normal().label(new Label().show(true)));
	    bar2.setItemStyle(itemStryle2);	
	    bar2.barWidth(5);
	    bar2.stack("总量");
	    bar2.data(320, 302, 341, 374, 390, 450, 420);
	    
	    Bar bar3 = new Bar("收入");
	        ItemStyle itemStryle3 = new ItemStyle();
	    	itemStryle3.normal(new Normal().label(new Label().show(true).position(Position.left)));
	    bar3.setItemStyle(itemStryle3);	
	    bar3.stack("总量");
	    bar3.data(-120, -132, -101, -134, -190, -230, -210);


        option.series(bar1, bar2, bar3);
        
        return option.toString();
	}
	
	public final static String generateObject_6() {
		GsonOption option = new GsonOption();
        option.title().text("收入-支出=利润").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    option.tooltip().formatter("function (params) {var tar;if (params[1].value != '-') {tar = params[1];}else {tar = params[0];}return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value;}");
	    
        option.legend("利润", "支出", "收入");
        
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.calculable(true);
        
        option.xAxis(new CategoryAxis().splitLine(new SplitLine().show(false)).data("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));
        
        option.yAxis(new ValueAxis());

        Bar bar1 = new Bar("辅助");
        bar1.stack("总量");
	        ItemStyle itemStryle1 = new ItemStyle();
	    	itemStryle1.normal(new Normal().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
	    	itemStryle1.emphasis(new Emphasis().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
	    bar1.setItemStyle(itemStryle1);	
        bar1.data(0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292);
        
	    Bar bar2 = new Bar("收入");
	        ItemStyle itemStryle2 = new ItemStyle();
	    	itemStryle2.normal(new Normal().label(new Label().show(true).position(Position.top)));
	    bar2.setItemStyle(itemStryle2);	
	    bar2.stack("总量");
	    bar2.data(900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-');
	    
	    Bar bar3 = new Bar("支出");
	        ItemStyle itemStryle3 = new ItemStyle();
	    	itemStryle3.normal(new Normal().label(new Label().show(true).position(Position.bottom)));
	    bar3.setItemStyle(itemStryle3);	
	    bar3.stack("总量");
	    bar3.data('-', '-', '-', 108, 154, '-', '-', '-', 119, 361, 203);

        option.series(bar1, bar2, bar3);
        
        return option.toString();
	}
	
	public final static String generateObject_7() {
		GsonOption option = new GsonOption();
        option.title().text("深圳月最低生活费组成（单位:元）").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.calculable(true);
        
        option.xAxis(new CategoryAxis().splitLine(new SplitLine().show(false)).data("总费用", "房租", "水电费", "交通费", "伙食费", "日用品数"));
        
        option.yAxis(new ValueAxis());

        Bar bar1 = new Bar("辅助");
        bar1.stack("总量");
	        ItemStyle itemStryle1 = new ItemStyle();
	    	itemStryle1.normal(new Normal().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
	    	itemStryle1.emphasis(new Emphasis().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
	    bar1.setItemStyle(itemStryle1);	
        bar1.data(0, 1700, 1400, 1200, 300, 0);
        
	    Bar bar2 = new Bar("收入");
	        ItemStyle itemStryle2 = new ItemStyle();
	    	itemStryle2.normal(new Normal().label(new Label().show(true).position(Position.inside)));
	    bar2.setItemStyle(itemStryle2);	
	    bar2.stack("总量");
	    bar2.data(2900, 1200, 300, 200, 900, 300);
	    
        option.series(bar1, bar2);
        
        return option.toString();
	}
	
	public final static String generateObject_8() {
		GsonOption option = new GsonOption();
        option.title().text("交错正负轴标签").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.calculable(true);
        
        	Grid grid = new Grid();
        	grid.setY(80);
        	grid.setY2(30);
        option.grid(grid);
        
        option.xAxis(new ValueAxis().position("top").splitLine(new SplitLine().lineStyle(new LineStyle().type(LineType.dashed))));
        
        option.yAxis(new CategoryAxis()
		        .axisLine(new AxisLine().show(false))
		        .axisLabel(new AxisLabel().show(false))
		        .axisTick(new AxisTick().show(false))
        		.splitLine(new SplitLine().show(false))
        		.data("ten", "nine", "eight", "seven", "six", "five", "four", "three", "two", "one"));

        Bar bar1 = new Bar("生活费");
        bar1.stack("总量");
	        ItemStyle itemStryle1 = new ItemStyle();
	    	itemStryle1.normal(new Normal().color("orange").borderRadius(5).label(new Label().show(true).position("left").formatter("{b}")));
	    bar1.setItemStyle(itemStryle1);	
	    	
        bar1.data(
        		new Data().value(-0.07).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(-0.09).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(0.2),
        		new Data().value(0.44),
        		new Data().value(-0.23).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(0.08).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(-0.17).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(0.47),
        		new Data().value(-0.36).itemStyle(new ItemStyle().normal(new Normal().label(new Label().show(true).position(Position.right)))),
        		new Data().value(0.18)
		);
        
	    
        option.series(bar1);
        
        return option.toString();
	}
	
	public final static String generateObject_9() {
		GsonOption option = new GsonOption();
        option.title().text("多维条形图").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    option.tooltip().formatter("{b}<br/>{a0}:{c0}%<br/>{a2}:{c2}%<br/>{a4}:{c4}%<br/>{a6}:{c6}%");
	    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.legend(new Legend().y(55).data("GML", "PYP", "WTC", "ZTW"));
        
        option.calculable(true);
        
        	Grid grid = new Grid();
        	grid.setY(80);
        	grid.setY2(30);
        option.grid(grid);
        
        option.xAxis(new ValueAxis().position("top").axisLabel(new AxisLabel().show(false)).splitLine(new SplitLine().show(false)));
        
        option.yAxis(new CategoryAxis()
        		.splitLine(new SplitLine().show(false))
        		.data("重庆", "天津", "上海", "北京"));
        
        ItemStyle placeHoledStyle = new ItemStyle();
        placeHoledStyle.normal(new Normal().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
        placeHoledStyle.emphasis(new Emphasis().barBorderColor("rgba(0,0,0,0)").color("rgba(0,0,0,0)"));
        
        ItemStyle dataStyle = new ItemStyle();
        dataStyle.normal(new Normal().label(new Label().show(true).position("insideLeft").formatter("{c}%")));

        Bar bar1 = new Bar("GML");
        bar1.stack("总量");
        bar1.data(38, 50, 33, 72);
	    bar1.setItemStyle(dataStyle);
	    
	    Bar bar2 = new Bar("GML");
        bar2.stack("总量");
        bar2.data(62, 50, 67, 28);
	    bar2.setItemStyle(placeHoledStyle);
	    
	    Bar bar3 = new Bar("PYP");
        bar3.stack("总量");
        bar3.data(61, 41, 42, 30);
	    bar3.setItemStyle(dataStyle);
	    
	    Bar bar4 = new Bar("PYP");
        bar4.stack("总量");
        bar4.data(39, 59, 58, 70);
	    bar4.setItemStyle(placeHoledStyle);
	    
	    Bar bar5 = new Bar("WTC");
        bar5.stack("总量");
        bar5.data(37, 35, 44, 60);
	    bar5.setItemStyle(dataStyle);
	    
	    Bar bar6 = new Bar("WTC");
        bar6.stack("总量");
        bar6.data(63, 65, 56, 40);
	    bar6.setItemStyle(placeHoledStyle);
	    
	    Bar bar7 = new Bar("ZTW");
        bar7.stack("总量");
        bar7.data(71, 50, 31, 39);
	    bar7.setItemStyle(dataStyle);
	    
	    Bar bar8 = new Bar("ZTW");
        bar8.stack("总量");
        bar8.data(71, 50, 31, 39);
	    bar8.setItemStyle(placeHoledStyle);
        
	    
        option.series(bar1, bar2, bar3, bar4, bar5, bar6, bar7, bar8);
        
        return option.toString();
	}
	
	public final static String generateObject_10() {
		GsonOption option = new GsonOption();
        option.title().text("温度计式图表").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer);
	    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.legend(new Legend().selectedMode(false).data("Acutal", "Forecast"));
        
        option.calculable(true);
        
        option.xAxis(new CategoryAxis().data("Cosco", "CMA", "APL", "OOCL", "Wanhai", "Zim"));
        option.yAxis(new ValueAxis().boundaryGap(0D	, 0.1D));
        
        Bar bar1 = new Bar("Acutal");
        bar1.stack("sum");
	    bar1.setItemStyle(new ItemStyle().normal(new Normal().color("tomato").barBorderColor("tomato").barBorderWidth(6).barBorderRadius(0).label(new Label().show(true).position("insideTop"))));
	    bar1.data(260, 200, 220, 120, 100, 80);
	    
	    Bar bar2 = new Bar("Forecast");
        bar2.stack("sum");
	    bar2.setItemStyle(new ItemStyle().normal(new Normal().color("#fff").barBorderColor("tomato").barBorderWidth(6).barBorderRadius(0).label(new Label().show(true).position("top").textStyle(new TextStyle().color("tomato")))));
	    bar2.data(40, 80, 50, 80,80, 70);
	    
        option.series(bar1, bar2);
        
        return option.toString();
	}
	
	public final static String generateObject_11() {
		GsonOption optionAll = new GsonOption();
		
		Timeline timeline = new Timeline();
		timeline.data( "2002","2003","2004","2005","2006","2007","2008","2009","2010","2011")
		.autoPlay(true).playInterval(1000);
		timeline.label().formatter("function(s) {return s.slice(0, 4);}");
		optionAll.timeline(timeline);
		
		
		GsonOption option1 = new GsonOption();
        option1.title().text("2002全国宏观经济指标").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	    option1.tooltip().trigger(Trigger.axis);
	    
        option1.toolbox().show(true).x("right").y("center").orient(Orient.vertical).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar, Magic.stack, Magic.tiled).show(true), Tool.restore, Tool.saveAsImage);
        
        option1.legend(new Legend().x("right").data("GDP","金融","房地产","第一产业","第二产业","第三产业").selected("GDP",true)
        		.selected("金融", false).selected("房地产", false).selected("第一产业", false).selected("第二产业", false).selected("第三产业", false));
        
        option1.calculable(true);
        
	        Grid grid = new Grid();
	    	grid.setY(80);
	    	grid.setY2(100);
    	option1.grid(grid);
        
        option1.xAxis(new CategoryAxis().axisLabel(new AxisLabel().interval(0)).data("北京","\n天津","河北","\n山西","内蒙古","\n辽宁","吉林","\n黑龙江","上海","\n江苏","浙江","\n安徽","福建","\n江西","山东","\n河南","湖北","\n湖南","广东","\n广西","海南","\n重庆","四川","\n贵州","云南","\n西藏","陕西","\n甘肃","青海","\n宁夏","新疆"));
        
        	List<Axis> _list = new ArrayList<Axis>();
        	_list.add(new ValueAxis().name("GDP（亿元）").max(53500));
        	_list.add(new ValueAxis().name("其他（亿元）"));
        option1.yAxis(_list);
        
        Bar bar1 = new Bar("GDP");
	    bar1.markLine(new MarkLine().symbol("['arrow','none']").symbolSize("[4, 2]").itemStyle(new ItemStyle().normal(new Normal().barBorderColor("orange").lineStyle(new LineStyle().color("orange")).label(new Label().position("left").textStyle(new TextStyle().color("orange")).formatter("function(params){return Math.round(params.value); }")))).data(new Data("平均值").type(MarkType.average)));
	    for (int i = 0; i < 31; i++) {
			bar1.data(TimeLineOption.generateData("GDP"));
		}
	    
	    Bar bar2 = new Bar("金融");
	    bar2.yAxisIndex(1);
	    for (int i = 0; i < 31; i++) {
			bar2.data(TimeLineOption.generateData("金融"));
		}
	    
	    Bar bar3 = new Bar("房地产");
	    bar3.yAxisIndex(1);
	    for (int i = 0; i < 31; i++) {
			bar3.data(TimeLineOption.generateData("房地产"));
		}
	    
	    Bar bar4 = new Bar("第一产业");
	    bar4.yAxisIndex(1);
	    for (int i = 0; i < 31; i++) {
			bar4.data(TimeLineOption.generateData("第一产业"));
		}
	    
	    Bar bar5 = new Bar("第二产业");
	    bar5.yAxisIndex(1);
	    for (int i = 0; i < 31; i++) {
	    	bar5.data(TimeLineOption.generateData("第二产业"));
		}
	    
	    Bar bar6 = new Bar("第三产业");
	    bar6.yAxisIndex(1);
	    for (int i = 0; i < 31; i++) {
	    	bar6.data(TimeLineOption.generateData("第三产业"));
		}
	    
        option1.series(bar1, bar2, bar3, bar4, bar5, bar6);
        
        //option2
        GsonOption option2 = new GsonOption();
        option2.title().text("2003全国宏观经济指标");
	        Bar bar21 = new Bar("GDP");
		    bar21.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar21.data(TimeLineOption.generateData("GDP"));
			}
        option2.series(bar21, bar2, bar3, bar4, bar5, bar6);
        
        //option3
        GsonOption option3 = new GsonOption();
        option3.title().text("2003全国宏观经济指标");
	        Bar bar31 = new Bar("GDP");
		    bar31.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar31.data(TimeLineOption.generateData("GDP"));
			}
        option3.series(bar31, bar2, bar3, bar4, bar5, bar6);
        
        //option4
        GsonOption option4 = new GsonOption();
        option4.title().text("2004全国宏观经济指标");
	        Bar bar41 = new Bar("GDP");
		    bar41.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar41.data(TimeLineOption.generateData("GDP"));
			}
        option4.series(bar41, bar2, bar3, bar4, bar5, bar6);
        
        //option5
        GsonOption option5 = new GsonOption();
        option5.title().text("2005全国宏观经济指标");
	        Bar bar51 = new Bar("GDP");
		    bar51.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar51.data(TimeLineOption.generateData("GDP"));
			}
        option5.series(bar51, bar2, bar3, bar4, bar5, bar6);
        
        //option6
        GsonOption option6 = new GsonOption();
        option6.title().text("2003全国宏观经济指标");
	        Bar bar61 = new Bar("GDP");
		    bar61.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar61.data(TimeLineOption.generateData("GDP"));
			}
        option6.series(bar61, bar2, bar3, bar4, bar5, bar6);
        
        //option7
        GsonOption option7 = new GsonOption();
        option7.title().text("20073全国宏观经济指标");
	        Bar bar71 = new Bar("GDP");
		    bar71.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar71.data(TimeLineOption.generateData("GDP"));
			}
        option7.series(bar71, bar2, bar3, bar4, bar5, bar6);
        
        //option8
        GsonOption option8 = new GsonOption();
        option8.title().text("2003全国宏观经济指标");
	        Bar bar81 = new Bar("GDP");
		    bar81.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar81.data(TimeLineOption.generateData("GDP"));
			}
        option8.series(bar81, bar2, bar3, bar4, bar5, bar6);
        
        //option9
        GsonOption option9 = new GsonOption();
        option9.title().text("2009全国宏观经济指标");
	        Bar bar91 = new Bar("GDP");
		    bar91.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar91.data(TimeLineOption.generateData("GDP"));
			}
        option9.series(bar91, bar2, bar3, bar4, bar5, bar6);
        
        //option10
        GsonOption option10 = new GsonOption();
        option10.title().text("2010全国宏观经济指标");
	        Bar bar101 = new Bar("GDP");
		    bar101.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar81.data(TimeLineOption.generateData("GDP"));
			}
        option10.series(bar101, bar2, bar3, bar4, bar5, bar6);
        
        //option11
        GsonOption option11 = new GsonOption();
        option11.title().text("2011全国宏观经济指标");
	        Bar bar111 = new Bar("GDP");
		    bar111.yAxisIndex(1);
		    for (int i = 0; i < 31; i++) {
		    	bar111.data(TimeLineOption.generateData("GDP"));
			}
        option11.series(bar111, bar2, bar3, bar4, bar5, bar6);
        
        optionAll.options(option1, option2, option3, option4, option5, option6, option7, option8, option9, option10, option11);
        
        
        return optionAll.toString();
	}
	
	
	public final static String generateObject_12() {
		GsonOption option = new GsonOption();
        option.title().text("ECharts2 vs ECharts1").subtext("数据来自网络(可以设定超链接)").sublink("http://www.baidu.com");
        
	    option.tooltip().trigger(Trigger.axis);
	    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        
        option.legend(new Legend().selectedMode(false).data("ECharts1 - 2k数据","ECharts1 - 2w数据","ECharts1 - 20w数据","", "ECharts2 - 2k数据","ECharts2 - 2w数据","ECharts2 - 20w数据"));
        
        option.calculable(true);
        
	        Grid grid = new Grid();
	    	grid.setY(70);
	    	grid.setY2(30);
	    	grid.setX2(20);
		option.grid(grid);
        
        
        option.xAxis(new CategoryAxis().data("Line","Bar","Scatter","K","Map"), 
        		new CategoryAxis().axisLine(new AxisLine().show(false)).axisTick(new AxisTick().show(false))
        		.axisLabel(new AxisLabel().show(false)).splitArea(new SplitArea().show(false)).splitLine(new SplitLine().show(false))
        		.data("Line","Bar","Scatter","K","Map"));
        
        option.yAxis(new ValueAxis().axisLabel(new AxisLabel().formatter("{value} ms")));
        
        Bar bar1 = new Bar("ECharts2 - 2k数据");
	    bar1.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(193,35,43,1)").label(new Label().show(true))));
	    bar1.data(40,155,95,75, 0);
	    
	    Bar bar2 = new Bar("ECharts2 - 2w数据");
	    bar2.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(181,195,52,1)").label(new Label().show(true).textStyle(new TextStyle().color("#27727B")))));
	    bar2.data(100,200,105,100,156);
	    
	    Bar bar3 = new Bar("ECharts2 - 20w数据");
	    bar3.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(252,206,16,1)").label(new Label().show(true).textStyle(new TextStyle().color("#E87C25")))));
	    bar3.data(100,200,105,100,156);
	    
	    Bar bar4 = new Bar("ECharts1 - 2k数据");
	    bar4.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(181,195,52,0.5)").label(new Label().show(true).formatter("function(p){return p.value > 0 ? (p.value +'\n'):'';}"))));
	    bar4.xAxisIndex(1);
	    bar4.data(96,224,164,124,0);
	    
	    Bar bar5 = new Bar("ECharts1 - 2w数据");
	    bar5.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(181,195,52,0.5)").label(new Label().show(true))));
	    bar5.xAxisIndex(1);
	    bar5.data(491,2035,389,955,347);
	    
	    Bar bar6 = new Bar("ECharts1 - 20w数据");
	    bar6.setItemStyle(new ItemStyle().normal(new Normal().color("rgba(252,206,16,0.5)").label(new Label().show(true).formatter("function(p){return p.value > 0 ? (p.value +'+'):'';}"))));
	    bar6.xAxisIndex(1);
	    bar6.data(3000,3000,2817,3000,0);
	    
        option.series(bar1, bar2, bar3, bar4, bar5, bar6);
        
        return option.toString();
	}
	
	public final static String generateObject_13() {
		GsonOption option = new GsonOption();
        option.title().text("双数值柱形图").subtext("纯属虚构");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.lineStyle(new LineStyle().width(1).type(LineType.dashed)).setType(PointerType.cross);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer).formatter("function (params) {return params.seriesName + ' : [ '+ params.value[0] + ', ' + params.value[1] + ' ]';}");
	    
        option.legend("数据一", "数据二");
        
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        option.calculable(true);
        option.xAxis(new ValueAxis());
        option.yAxis(new ValueAxis().axisLine(new AxisLine().lineStyle(new LineStyle().color("#dc143c"))));

        Bar bar = new Bar("'数据1");
        bar.data(new double[]{1.5,10}).data(new double[]{5,7}).data(new double[]{8, 8})
        .data(new double[]{12, 6}).data(new double[]{11, 12})
        .data(new double[]{16, 9}).data(new double[]{14, 6}).data(new double[]{17, 4});
        
        bar.markPoint().data(
        		new Data("最大值").type(MarkType.max).symbol("emptyCircle").itemStyle(new ItemStyle().normal(new Normal().color("#dc143c").label(new Label().position(Position.top)))),
        		new Data("最小值").type(MarkType.min).symbol("emptyCircle").itemStyle(new ItemStyle().normal(new Normal().color("#dc143c").label(new Label().position(Position.bottom)))),
        		
        		new Data("最大值").type(MarkType.max).valueIndex(0).symbol("emptyCircle").itemStyle(new ItemStyle().normal(new Normal().color("#1e90ff").label(new Label().position(Position.top)))),
        		new Data("最小值").type(MarkType.min).valueIndex(0).symbol("emptyCircle").itemStyle(new ItemStyle().normal(new Normal().color("#1e90ff").label(new Label().position(Position.bottom))))
        );
        
        bar.markLine().data(
        		new Data("最大值").type(MarkType.max).itemStyle(new ItemStyle().normal(new Normal().color("#dc143c"))),
        		new Data("最小值").type(MarkType.min).itemStyle(new ItemStyle().normal(new Normal().color("#dc143c"))),
        		new Data("平均值").type(MarkType.average).itemStyle(new ItemStyle().normal(new Normal().color("#dc143c"))),
        		
        		new Data("最大值").type(MarkType.max).valueIndex(0).itemStyle(new ItemStyle().normal(new Normal().color("#1e90ff"))),
        		new Data("最小值").type(MarkType.min).valueIndex(0).itemStyle(new ItemStyle().normal(new Normal().color("#1e90ff"))),
        		new Data("平均值").type(MarkType.average).valueIndex(0).itemStyle(new ItemStyle().normal(new Normal().color("#1e90ff")))
		);

        
        Bar bar2 = new Bar("数据2");
        bar2.barMinHeight(10).barMinHeight(10);
        bar2.data(new double[]{1, 2}).data(new double[]{2,3}).data(new double[]{4, 4})
        .data(new double[]{7, 5}).data(new double[]{11, 11})
        .data(new double[]{18, 15});

        option.series(bar, bar2);
        
        return option.toString();
	}
	
	
	public final static String generateObject_14() {
		GsonOption option = new GsonOption();
        option.title().x("center").text("ECharts例子个数统计").subtext("纯属虚构");
        
	    option.tooltip().trigger(Trigger.item);
	    
		    Grid grid = new Grid();
	    	grid.setY(80);
	    	grid.setY2(60);
		option.grid(grid);
    
        option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
        option.calculable(true);
        
        option.xAxis(new CategoryAxis().show(false).data("Line", "Bar", "Scatter", "K", "Pie", "Radar", "Chord", "Force", "Map", "Gauge", "Funnel"));
        option.yAxis(new ValueAxis().show(false));

        Bar bar = new Bar("'ECharts例子个数统计");
        bar.itemStyle(new ItemStyle().normal(new Normal()
        .color("function(params) {var colorList = ['#C1232B','#B5C334','#FCCE10','#E87C25','#27727B','#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD','#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0' ];return colorList[params.dataIndex]}")
        .label(new Label().show(false).position(Position.top).formatter("{b}\n{c}"))));
        bar.data(12,21,10,4,12,5,6,5,25,23,7);
        
        bar.data(new Data("Line").xAxis(0).y(350).symbolSize(20));

        option.series(bar);
        
        return option.toString();
	}
	
	public final static String generateObject_15() {
		
		GsonOption option = new GsonOption();
        option.title().text("2010-2013年中国城镇居民家庭人均消费构成（元）").subtext("纯属虚构");
        
	        AxisPointer axisPointer = new AxisPointer();
	    	axisPointer.setType(PointerType.shadow);
	    option.tooltip().trigger(Trigger.axis).axisPointer(axisPointer).backgroundColor("rgba(255,255,255,0.7)");
		   
	    option.legend(new Legend().x("right").data("2010","2011","2012","2013"));
	    
	    option.toolbox().show(true).feature(Tool.mark, Tool.dataView, new MagicType(Magic.line, Magic.bar).show(true), Tool.restore, Tool.saveAsImage);
	      
	    option.calculable(true);
	    
		    Grid grid = new Grid();
	    	grid.setY(80);
	    	grid.setY2(40);
	    	grid.setX2(40);
		option.grid(grid);
		
		option.xAxis(new CategoryAxis().show(false).data("食品", "衣着", "居住", "家庭设备及用品", "医疗保健", "交通和通信", "文教娱乐服务", "其他"));
		option.yAxis(new ValueAxis());
		
		
		Bar bar1 = new Bar("2010");
        bar1.data(4804.7,1444.3,1332.1,908,871.8,1983.7,1627.6,499.2);
        
        Bar bar2 = new Bar("2011");
        bar2.data(5506.3,1674.7,1405,1023.2,969,2149.7,1851.7,581.3);
        
        Bar bar3 = new Bar("2012");
        bar3.data(6040.9,1823.4,1484.3,1116.1,1063.7,2455.5,2033.5,657.1);
        
        Bar bar4 = new Bar("2013");
        bar4.data(6311.9,1902,1745.1,1215.1,1118.3,2736.9,2294,699.4);
        
        option.series(bar1,bar2,bar3,bar4);
        
        return option.toString();
	}
}
