package com.alexgaoyh.echarts.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alexgaoyh.echarts.test.bar.util.BarTestJSONUtil;

@Controller
public class WebSocketController {

	//-----------------------服务端向客户端发送消息---------------------------------------------------------- begin
	private final MessageSendingOperations<String> messagingTemplate;

    @Autowired
    public WebSocketController(
        final MessageSendingOperations<String> messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }
    
    @RequestMapping("/init")
	public ModelAndView start(ModelAndView model) {
    	String optionJSON = BarTestJSONUtil.generateObject_1();
    	model.addObject("optionJSON", optionJSON);
    	model.setViewName("jsp/init");
    	return model;
	}
    
    @MessageMapping("/initJson")
	@SendTo("/topic/showResult")
	public void initJson() throws Exception {
    	
	}
    
    @RequestMapping("/send")
    @ResponseBody
	public void send() {
    	this.messagingTemplate.convertAndSend(
                "/topic/showResult", BarTestJSONUtil.generateObject_2());
    }
  //-----------------------服务端向客户端发送消息---------------------------------------------------------- end
}
