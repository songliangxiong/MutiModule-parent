<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Manager</title>

	<jsp:include page="../common/adminCommon.jsp"></jsp:include>	

	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/autocomplete/jquery.autocomplete.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/autocomplete/jquery.autocomplete.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/json/ssq.json"></script>
	
</head>
<body class="easyui-layout" >
	<div  data-options="region:'north'" class="search" id ="search" style="height: 50px;">
		名称： <input id="searchName" name="searchName" />
		<a id="search-btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'"   >search</a>
	</div>
	
	<div data-options="region:'center',split:false">
		<table id="dg-1" class="easyui-datagrid" title="列表" style="width: 700px; height: 300px"
			data-options="toolbar:'#toolbar-1',checkOnSelect:true,selectOnCheck:true,fit:true,rownumbers:true,fitColumns:true,url:'${pageContext.request.contextPath}/${moduleName}/getData',method:'get',pagination:true">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th data-options="field:'id',width:80">编码</th>
					<th data-options="field:'name',width:80">用户名</th>
					<th data-options="field:'createTime',width:80,formatter:function(value){var date=parseDate(value);return date;}">创建时间</th>
					<th data-options="field:'deleteFlag',width:80,formatter:function(value){
							if(value=='NORMAL'){
								return '未删除';
							}else if(value=='DELETE'){
									return '已删除';
							}
					}">删除状态</th>
				</tr>
			</thead>
		</table>
		
		<div id="toolbar-1">
			<a href="#" class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a> 
			<a href="#" class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">修改</a> 
			<a href="#" class="easyui-linkbutton remove" iconCls="icon-remove" plain="true">删除</a>
			<a href="#" class="easyui-linkbutton releation" iconCls="icon-add" plain="true">包含角色</a>
		</div>
		
		<div id="dlg-1" class="easyui-dialog" title="数据参数" style="z-Index: 1px;" fit="true" closed="true" buttons="#dlg-buttons-1">
			<form method="post">
				<table cellpadding="5">
					<tr><td>如用户未更改密码，默认密码为admin</td></tr>
					<tr>
						<td><input type="hidden" name="id" /></td>
					</tr>
		    		<tr>
		    			<td>用户名:</td>
		    			<td><input class="easyui-textbox" type="text" name="name" data-options="required:true"></input></td>
		    		</tr>
		    		<tr>
		    			<td>地区省市区地址:</td>
		    			<td><input type="text" name="startPosNames" id="startPosNames"></input></td>
		    		</tr>
		    		<tr>
		    			<td>地区省市区ids集合:</td>
		    			<td><input type="text" name="startPosIds" id="startPosIds" readonly="readonly"></input></td>
		    		</tr>
		    	</table>
			</form>
		</div>
		
		<div id="dlg-buttons-1">
			<a href="#" class="easyui-linkbutton  save" iconCls="icon-ok">保存</a> 
			<a href="#" class="easyui-linkbutton cancel" iconCls="icon-cancel">取消</a>
		</div>
	</div>
	
	<script type="text/javascript">
	
		$(document).ready(function(){
			
			$('#startPosNames').AutoComplete({
                'data': countries,
                'listStyle': 'custom',
                'emphasis': false,
                'createItemHandler': function(index, data){
                    var div = $("<div>"+data.label+"</div>");
                    return div;
                },
                'afterSelectedHandler': function() {
                	$('#startPosIds').val($(this)[0].searchView.find('li.selected').data('data').ids);
                }
            }).AutoComplete('show');
			
		});
	
		$( function() {
			var dg1 = new DataGridEasyui(context_, 1 , templateUrl, 'crud');
			
			$.extend(dg1, {
				init : function() {
					DataGridEasyui.prototype.init.call(this);
					//add 关联关系处理 begin
					this.toolBar.find(".releation").bind('click', this.proxy(function(){
						var rows = this.dataGrid.datagrid('getSelections');
						if (!rows || rows.length == 0) {
							$.messager.alert('提示', '请选择记录！');
						} else {
							if (rows.length == 1) {
								document.location.href = this.context + "/sysmanUserRoleRel/list?id="+rows[0].id;
							} else {
								$.messager.alert('提示', '请选择单行记录！');
							}
						}
					},this,this.toolBar.find(".releation")));
					//add 关联关系处理 end
				}
			});
			dg1.init();
			
			$("#search-btn").on('click',function (){
				$("#dg-1").datagrid('load',{
					searchName : $("#searchName").val()
				});
			});
			
		});
	</script>
	
</body>
</html>