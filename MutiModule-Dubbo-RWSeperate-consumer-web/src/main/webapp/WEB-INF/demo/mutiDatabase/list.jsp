<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
    <!-- Data Tables -->
    <link href="/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Basic Data Tables example with responsive plugin</h5>
			</div>
			<div class="ibox-content">

				<div class="dataTables_wrapper form-inline">
					<div>
						<label>
							Search:
							<input type="search" name="listSearchName" id="listSearchName" value="${listSearchName }" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0">
						</label>
					</div>
					<div class="clear"></div>
					<div class="DTTT_container">
						<button class="btn btn-primary" type="button" onclick="onclickSearch($('#myPageJSPPagination li.active').val());">搜索</button>
						<button class="btn btn-primary" type="submit" onclick="location.href='${context_ }/${moduleName }/add'">添加</button>
					</div>
					
					<table style="overflow-x: scroll;" class="table table-striped table-bordered table-hover dataTables-example dataTable dtr-inline" >
						<thead>
							<tr role="row">
								<th rowspan="1" colspan="1">Rendering engine</th>
								<th rowspan="1" colspan="1">Browser</th>
								<th rowspan="1" colspan="1">Platform(s)</th>
								<th rowspan="1" colspan="1">Engine version</th>
								<th rowspan="1" colspan="1">CSS grade</th>
								<th rowspan="1" colspan="1">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${pageView.records}" var="data" varStatus="loop">
								<tr <c:if test="${loop.index%2 == 0}"> class = "gradeA odd "</c:if><c:if test="${loop.index%2 == 0}"> class = "gradeA even "</c:if> role="row">
									<td class="sorting_1">${data.id }</td>
									<td class="center"><a href="${context_ }/${moduleName }/show/${data.id }">${data.name }</a></td>
									<td>${data.id }</td>
									<td class="center">${data.name }</td>
									<td class="center">${data.id }</td>
									<td class="center"><button class="btn btn-info " type="button" onclick="onclickChange($('#myPageJSPPagination li.active').val(), '${data.id}');"><i class="fa fa-paste"></i>修改</button></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">Rendering engine</th>
								<th rowspan="1" colspan="1">Browser</th>
								<th rowspan="1" colspan="1">Platform(s)</th>
								<th rowspan="1" colspan="1">Engine version</th>
								<th rowspan="1" colspan="1">CSS grade</th>
								<th rowspan="1" colspan="1">操作</th>
							</tr>
						</tfoot>
					</table>
					
					<c:import url="../../common/dataTable/myTableLengthJSP.jsp"></c:import>
					<br></br>
					<!-- 将分页JSP包含进来 -->
					<%@ include file="../../common/dataTable/myPageJSP.jsp"%>
				</div>

			</div>
		</div>
	</div>
    
    <!-- Data Tables -->
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function() {
	        $('.dataTables-example').dataTable({
	            responsive: false,
	            paginate: false,
	            info: false,
	            searching: false,
	            bAutoWidth: false,
	            aaSorting: [[ 0, "desc" ]]
	        });
		});
	
		var totalPageVar = '${pageView.totalpage}';	
	
		function onclickSearch(currentPage) {
			if(currentPage <= 1) {
				currentPage = 1;
			}
			if(currentPage >= totalPageVar ) {
				currentPage = totalPageVar;
			}
			StandardPost('${context_}/${moduleName}/list',
				{
					currentPage: currentPage,
					recordPerPage: $('#myTableLengthJspSelect').val(),
					listSearchName: $('#listSearchName').val()
				}
			);
		};
		
		function onclickChange(currentPage, id) {
			if(currentPage <= 1) {
				currentPage = 1;
			}
			if(currentPage >= totalPageVar ) {
				currentPage = totalPageVar;
			}
			StandardPost('${context_}/${moduleName}/edit/' + id,
				{
					currentPage: currentPage,
					recordPerPage: $('#myTableLengthJspSelect').val(),
					listSearchName: $('#listSearchName').val()
				}
			);
		};
		
		
	</script>
</body>
</html>