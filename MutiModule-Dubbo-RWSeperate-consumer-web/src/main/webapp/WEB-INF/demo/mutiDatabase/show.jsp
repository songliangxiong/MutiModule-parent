<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doSave">
					<jsp:include page="form.jsp"></jsp:include>
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$( document ).ready(function() {
			disableForm('formHTML', true);
		});
		
		//禁用form表单中所有的input[文本框、复选框、单选框],select[下拉选],多行文本框[textarea]
		function disableForm(formId,isDisabled) {
		    
		    var attr="disable";
			if(!isDisabled){
			   attr="enable";
			}
			$("form[id='"+formId+"'] :text").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] textarea").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] select").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :radio").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :checkbox").attr("disabled",isDisabled);

			/* $("#" + formId + " input[class='combobox-f combo-f']").each(function () {
				if (this.id) {alert("input"+this.id);
					$("#" + this.id).combobox(attr);
				}
			});
			$("#" + formId + " select[class='combobox-f combo-f']").each(function () {
				if (this.id) {
					$("#" + this.id).combobox(attr);
				}
			});
			$("#" + formId + " input[class='datebox-f combo-f']").each(function () {
				if (this.id) {
					$("#" + this.id).datebox(attr);
				}
			}); */
		}
	</script>
</body>
</html>