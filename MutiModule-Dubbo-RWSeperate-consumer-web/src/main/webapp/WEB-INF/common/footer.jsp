<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="footer">
		<div class="pull-right">
			10GB of <strong>250GB</strong> Free.
		</div>
		<div>
			<strong>Copyright</strong> Example Company © 2014-2015
		</div>
	</div>
</body>
</html>