<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<body>
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav" id="side-menu">
				<jsp:include page="nav-header.jsp"></jsp:include>
				
				<c:forEach var="data" items="${treeList}" varStatus="vs">
					<li class="my-tree-li" id="li-${data.id }">
						<a href="javaScript:void(0);" onclick="myLeftOpenHref('/${data.href }', '${data.id }');" aria-expanded="false" id="${data.id }">
							<i class="fa fa-sitemap"></i>
							<span class="nav-label">${data.text } </span>
							<c:if test="${fn:length(data.children) > 0}">
								<span class="fa arrow"></span>
							</c:if>
						</a>
						<c:if test="${fn:length(data.children) > 0}">
							<c:forEach var="data1" items="${data.children}" varStatus="vs1">
								<ul class="nav nav-second-level" id="ul-nav-second-${data.id }-${data1.id} ">
									<li id="li-nav-second-${data.id }-${data1.id}">
										<a href="javaScript:void(0);" onclick="myLeftOpenHref('/${data1.href }', '${data.id }');" aria-expanded="false" id="${data1.id }">${data1.text } 
											<c:if test="${fn:length(data1.children) > 0}">
												<span class="fa arrow"></span>
											</c:if>
										</a>
										<c:if test="${fn:length(data1.children) > 0}">
											<ul class="nav nav-third-level" id="ul-nav-third-${data.id }-${data1.id}">
												<c:forEach var="data2" items="${data1.children}" varStatus="vs2">
													<li><a href="javaScript:void(0);" onclick="myLeftOpenHref('/${data2.href }', '${data.id }-${data1.id }-${data2.id }');" id="${data2.id }">${data2.text }</a></li>
												</c:forEach>
											</ul>
										</c:if>
									</li>
								</ul>
							</c:forEach>
						</c:if>
					</li>
				</c:forEach>
			</ul>

		</div>
	</nav>
	<script type="text/javascript">
		function myLeftOpenHref(href, id) {
			if(href != '/#') {
				window.location.href = context_ + href;
			}
		}
	</script>
</body>
</html>