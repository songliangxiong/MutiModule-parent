<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
	<div class="row">
		<div class="col-lg-3">
		   <div class="ibox float-e-margins">
		       <div class="ibox-content mailbox-content">
		           <div class="file-manager">
		               <div class="space-25"></div>
		               <h5>数据权限规则</h5>
		               <ul class="folder-list m-b-md" style="padding: 0">
							<c:forEach var="ruleVO" items="${sqlDataAuthorityRuleVOList}" varStatus="vs">
								<li <c:if test="${ruleVO.classPath == classPath }">class="my-font-weight-900"</c:if>>
									<a href="javaScript:void(0);" onclick="openRightContentByClasspath('${ruleVO.classPath}');">
										<i class="fa fa-envelope-o"></i> ${ruleVO.remarks }
									</a>
								</li>
							</c:forEach>
		                </ul>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		    </div>
		</div>
		<!--  -->
		<c:if test="${not empty classPath}">
			<div class="col-lg-9 animated fadeInRight">
				<div class="mail-box-header">
					<div class="mail-box">
						<h2 id="dataSourceTypeNameId">
							<c:forEach var="ruleVO" items="${sqlDataAuthorityRuleVOList}" varStatus="vs">
								<c:if test="${ruleVO.classPath == classPath }">${ruleVO.remarks }</c:if>
							</c:forEach>
							<a class="btn btn-primary stat-percent" href="javaScript:void(0);" onclick="addDataAuthorityRule();">添加</a>
						</h2>
						<table class="table table-hover table-mail">
							<thead>
								<tr>
									<th>名称</th>
									<th>描述</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${pageView.records}" var="data" varStatus="loop">
									<tr class="read">
										<td class="center"><a style="color: blue;"
											href="${context_ }/${moduleName }/show/${data.id} ">${data.name }</a></td>
										<td class="center">${data.remarks }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<c:import url="../../../../common/dataTable/myTableLengthJSP.jsp"></c:import>
						<br></br>
						<!-- 将分页JSP包含进来 -->
						<c:import url="../../../../common/dataTable/myPageJSP.jsp"></c:import>
	
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<div class="col-lg-9 animated fadeInRight"></div>
	<script type="text/javascript">
		<!--
			var _classPathVar = '${classPath}';	
			var _currentPageStr = '${pageView.currentpage}';
			var _recordPerPageStr = '${pageView.recordPerPage}';
			var totalPageVar = '${pageView.totalpage}';
			
			function openRightContentByClasspath(classPath) {
				var arrUrl = window.location.href.split('?');
				window.location.href = arrUrl[0] + '?classPath=' + classPath;
			}
		
			function addDataAuthorityRule() {
				window.location.href = '${context_ }/${moduleName }' + '/add?classPath=' + _classPathVar
						+ "&currentPage=" + _currentPageStr + "&recordPerPage=" + _recordPerPageStr;
			}
			
			function onclickSearch(currentPage) {
				if(currentPage <= 1) {
					currentPage = 1;
				}
				if(currentPage >= totalPageVar ) {
					currentPage = totalPageVar;
				}
				StandardPost('${context_}/${moduleName}/list',
					{
						currentPage: currentPage,
						recordPerPage: $('#myTableLengthJspSelect').val(),
						classPath: _classPathVar,
						listSearchName: $('#listSearchName').val()
					}
				);
			};
		//-->
	</script>
</html>