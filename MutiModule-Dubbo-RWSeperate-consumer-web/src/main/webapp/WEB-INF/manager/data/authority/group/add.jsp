<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doSaveView">
					<jsp:include page="form.jsp"></jsp:include>
					<c:forEach var="item" items="${operationViewMapWithClassPath}">   
						<div class="form-group"><label class="col-sm-2 control-label">${item.key } <br/></label>
						<div class="col-sm-10">
							<c:forEach var="itemValue" items="${item.value }">
								<div class="checkbox i-checks"><label> <input type="checkbox" name="ruleId" value="${itemValue.id }"> <i></i> ${itemValue.name } </label></div>
							</c:forEach>
						</div>
					</div>
					</c:forEach>
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- jquery validate scripts -->
	<script src="/js/plugins/validate/jquery.validate.js"></script>
	<script src="/js/plugins/validate/additional-methods.js"></script>
	<script src="/js/${moduleName }/form-validate.js"></script>
</body>
</html>