<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form class="form-horizontal">
					<div class="form-group" >
						<label class="col-sm-2 control-label">IP地址</label>
						<div class="col-sm-10">
							<input type="text" id="ipAddress" name="ipAddress" value=""
								class="form-control">
						</div>
					</div>
					<div class="form-group" >
						<label class="col-sm-2 control-label">IP地址所属地区</label>
						<div class="col-sm-10">
							<input type="text" id="ipAddressRegion" name="ipAddressRegion" value="" disabled="disabled"
								class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<a class="btn btn-primary" type="button" onclick="ip2RegionClick();">查询</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		function ip2RegionClick() {
			$.ajax({
			   url: '${context_}/${moduleName}/getRegionByIP',
			   data : {ipAddress : $('#ipAddress').val()},
			   dataType :'json',
			   success: function(data){
				  $('#ipAddressRegion').val(data);
			   }
			});
		}
		
		//-->
	</script>
</html>