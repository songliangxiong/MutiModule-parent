package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.aop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.MutiModule.common.utils.CookieUtilss;
import com.MutiModule.common.utils.DubboRWSepConWebPropUtilss;
import com.MutiModule.common.vo.TreeNode;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alibaba.dubbo.config.annotation.Reference;

@Aspect
public class LeftMenuDataAop {
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService")
	private ISysmanResourceReadService sysmanResourceReadService;
	

	@Pointcut("within(com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseEmptyController+ )")
	public void controller() {
		
	}
	
	@Around("controller()")  
    public Object leftMenuData(ProceedingJoinPoint point) throws Throwable{  
		
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		//從web.xml裡面，取出context-param鍵值對標註的某個值
        String adminLoginCookieName = DubboRWSepConWebPropUtilss.getAdminCookie();
        
        String loginCookie = CookieUtilss.get(request, adminLoginCookieName);
		
		List<TreeNode> treeList = sysmanResourceReadService.selectTreeNodeListByUserId(loginCookie);
		request.setAttribute("treeList", treeList);
		
		String path = request.getRequestURI().replace(request.getContextPath(), "").replaceFirst("/", "");
		System.out.println(path);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("href", path);
		List<SysmanResource> _resourceList = sysmanResourceReadService.selectListByMap(map);
		if(_resourceList != null && !_resourceList.isEmpty() && _resourceList.size() > 0) {
			String parentIds = _resourceList.get(0).getParentIds();
			parentIds = parentIds + _resourceList.get(0).getId();
			
			// 左侧菜单栏部分的 回显操作 
			parentIds = parentIds.replace(",", "-");
			request.setAttribute("leftMenuId", parentIds);
			
			// 此链接用户所包含的资源 按钮 list集合	用户是否有这个按钮的操作权限
			List<SysmanResource> _iconsContainsList = sysmanResourceReadService.selectIconResourceByUserIdAndParentId(loginCookie, _resourceList.get(0).getId()+"");
			request.setAttribute("iconContainsList", _iconsContainsList);
		}
		
        return point.proceed();  
    }  

}
