package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.util;

import java.util.ArrayList;
import java.util.List;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.vo.DemoEntity;

public class DemoEntityVOUtilss {

	public static List<DemoEntity> generateList() {
		List<DemoEntity> _list = new ArrayList<DemoEntity>();
		for (int i = 0; i < 159; i++) {
			DemoEntity _demo = new DemoEntity();
			_demo.setId(IdWorkerInstance.getId());
			_demo.setName("alexgaoyh_" + (i+1));
			_demo.setRemark("remark_" + (i+1));
			
			_list.add(_demo);
		}
		
		return _list;
	}
}
