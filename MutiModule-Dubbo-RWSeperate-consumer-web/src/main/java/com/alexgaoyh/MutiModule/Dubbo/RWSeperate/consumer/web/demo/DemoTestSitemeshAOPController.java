package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.vo.DemoEntity;

@Controller
@RequestMapping(value="manager/sitemeshAOPTest")
public class DemoTestSitemeshAOPController extends BaseController<DemoEntity>{

	@RequestMapping(value = "/index")
	public ModelAndView index(ModelAndView model, HttpServletRequest request) {
		
		model.setViewName("demo/sitemeshAOPTest/index");
		return model;
	}
}